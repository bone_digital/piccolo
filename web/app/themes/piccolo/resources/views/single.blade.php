@extends('layouts.app')

@section('content')

	@while (have_posts()) @php the_post() @endphp

		<section class="module module-news-listing module-padded-top--double module-padded-btm--single">
			<div class="row module-pb-first">
				<div class="col sm-col-4 lg-col-12 scrolled-block">
					<h1 class="type-h2 scrolled-block__elem">{!! $post['title'] !!}</h1>
				</div>
			</div>
		</section>

		@include('partials.page-builder')

		<section class="module module-padded-top--single module-padded-btm--double">
			<div class="row scrolled-block">
				<div class="col sm-col-4 lg-col-12 scrolled-block__elem content">
				<a href="{!! $back_to_news !!}" class="type-h2">View more news</a>
				</div>
			</div>
		</section>

	@endwhile
	
@endsection
