@extends('layouts.app')

@section('content')

	@while (have_posts()) @php the_post() @endphp
        <section class="module module-full_screen_media">
            @if ($collection['banner_media_type'] == 'Image' AND $collection['banner_mobile_media_type'] == 'Image')
            <picture class="module-full_screen_media__media module-full_screen_media__media--image">
                <source srcset="{!! $collection['banner_image']['sizes']['large'] !!}" media="(min-width: 901px)">
                <img class="lazyimage lazyload" data-src="{!! $collection['banner_mobile_image']['sizes']['medium'] !!}" width="{!! $collection['banner_mobile_image']['sizes']['medium-width'] !!}" height="{!! $collection['banner_mobile_image']['sizes']['medium-height'] !!}" alt="{!! $collection['banner_mobile_image']['alt'] !!}" />
            </picture>
            @else
            
                @if ($collection['banner_media_type'] == 'Image')
                <img data-src="{!! $collection['banner_image']['sizes']['large'] !!}" width="{!! $collection['banner_image']['sizes']['large-width'] !!}" height="{!! $collection['banner_image']['sizes']['large-height'] !!}" alt="{!! $collection['banner_image']['alt'] !!}" class="lazyimage lazyload module-full_screen_media__media module-full_screen_media__media--image{{ $collection['banner_mobile_media_type'] != 'None' ? " sm-hide" : "" }}" />
                @endif

                @if ($collection['banner_media_type'] == 'Video')
                <video class="module-full_screen_media__media module-full_screen_media__media--video{{ $collection['banner_mobile_media_type'] != 'None' ? " sm-hide" : "" }}" autoplay muted loop playsinline src="{!! $collection['banner_video']['url'] !!}"></video>
                @endif

                @if ($collection['banner_media_type'] == 'Video URL')
                <video class="module-full_screen_media__media module-full_screen_media__media--video{{ $collection['banner_mobile_media_type'] != 'None' ? " sm-hide" : "" }}" autoplay muted loop playsinline src="{!! $collection['banner_video_url'] !!}"></video>
                @endif

                @if ($collection['banner_mobile_media_type'] == 'Image')
                <img data-src="{!! $collection['banner_mobile_image']['sizes']['medium'] !!}" width="{!! $collection['banner_mobile_image']['sizes']['medium-width'] !!}" height="{!! $collection['banner_mobile_image']['sizes']['medium-height'] !!}" alt="{!! $collection['banner_mobile_image']['alt'] !!}" class="lazyimage lazyload module-full_screen_media__media module-full_screen_media__media--image lg-hide" />
                @endif

                @if ($collection['banner_mobile_media_type'] == 'Video')
                <video class="module-full_screen_media__media--video lg-hide" autoplay muted loop playsinline src="{!! $collection['banner_mobile_video']['url'] !!}"></video>
                @endif

                @if ($collection['banner_mobile_media_type'] == 'Video URL')
                <video class="module-full_screen_media__media--video lg-hide" autoplay muted loop playsinline src="{!! $collection['banner_mobile_video_url'] !!}"></video>
                @endif

            @endif
            
            <div class="row module-full_screen_media__scroll-text-wrap">
                <div class="col sm-col-4 lg-col-12">
                    <span class="type-upper type-h5 module-full_screen_media__scroll-text-text">Scroll</span>
                </div>
            </div>
            <div class="module-full_screen_media__grad" aria-hidden="true"></div>
        </section>

		<section class="module module-collection module-padded-top--double module-padded-btm--single">
			<div class="row scrolled-block">
				<div class="col sm-col-4 lg-col-12 module-collection__intro">
                    <h1 class="type-h2 module-collection__intro-title scrolled-block__elem">{!! $collection['title'] !!}</h1>
                    @if ($collection['sub_heading'])
                    <h2 class="type-h6 type-upper scrolled-block__elem">{!! $collection['sub_heading'] !!}</h2>
                    @endif
                </div>
            </div>
            @if ($collection['description'])
            <div class="row scrolled-block">
                <div class="col sm-col-4 lg-col-6">
                    <p class="type-upper type-h6 scrolled-block__elem">{!! $collection['description'] !!}</p>
				</div>
			</div>
            @endif

            @if ($collection['awards'])
            <div class="row module-collection__awards module-padded-top--double scrolled-block">
                <div class="col sm-col-2 lg-col-2 lg-col-offset-4 module-collection__awards-col--top module-collection__awards-col module-collection__awards-col--title scrolled-block__elem">
                    <h3 class="type-p">{!! $collection['awards_heading'] !!}</h3>
                </div>

                @foreach ($collection['awards'] as $year)
                    @if ($year['display_year'])
                        <div class="col {{ (!$loop->first) ? 'sm-col-4' : 'sm-col-2' }} lg-col-1{{ (!$loop->first) ? ' lg-col-offset-6' : '' }} module-collection__awards-col--top module-collection__awards-col module-collection__awards-col--year scrolled-block__elem">
                            <h4 class="type-p">{!! $year['year'] !!}</h4>
                        </div>

                        @foreach ($year['awards'] as $award)
                            @if ($award['display_award_on_collection_page'])
                                <div class="col sm-col-4 lg-col-2{{ (!$loop->first) ? ' lg-col-offset-7' : ' module-collection__awards-col--top' }} module-collection__awards-col module-collection__awards-col--award scrolled-block__elem">
                                    @if ($award['link'])
                                    <a href="{!! $award['link'] !!}" class="module-award_list__item-awards-link">
                                    @endif
                                        <p class="type-p">{!! $award['award'] !!}</p>
                                    @if ($award['link'])
                                    </a>
                                    @endif
                                </div>
                                <div class="col sm-col-4 lg-col-3{{ (!$loop->first) ? '' : ' module-collection__awards-col--top' }} module-collection__awards-col module-collection__awards-col--body scrolled-block__elem">
                                @if ($award['link'])
                                    <a href="{!! $award['link'] !!}" class="module-award_list__item-awards-link">
                                    @endif
                                        <p class="type-p">{!! $award['awarding_body'] !!}</p>
                                    @if ($award['link'])
                                        <span class="module-award_list__item-awards-link-icon" aria-hidden="true"></span>
                                    </a>
                                    @endif
                                </div>
                            @endif
                        @endforeach
                    @endif
                @endforeach
                   
			</div>
            @endif
		</section>

		@include('partials.page-builder')

        @if ($collection_prev OR $collection_next)
        <section class="module module-single-nav module-padded-top--single module-padded-btm--double">
            <div class="module-single-nav__row-wrap scrolled-block">
                <div class="row module-single-nav__row scrolled-block__elem">
                    @if ($collection_prev)
                    <div class="col sm-col-2 lg-col-6 module-single-nav__col module-single-nav__col--prev scrolled-block__elem">
                        @if ($collection_prev['banner_image'])
                        <a href="{!! $collection_prev['link'] !!}" class="module-single-nav__image-link" data-hover="true" data-hovertext="View">
                            <figure class="lazy-container module-single-nav__image">
                                <img data-src="{!! $collection_prev['banner_image']['sizes']['medium'] !!}" width="{!! $collection_prev['banner_image']['sizes']['medium-width'] !!}" height="{!! $collection_prev['banner_image']['sizes']['medium-height'] !!}" alt="{!! $collection_prev['title'] !!}" class="lazyimage lazyload" />
                            </figure>
                            <h2 class="module-single-nav__title type-h6 type-upper">{!! $collection_prev['title'] !!}</h2>
                        </a>
                        @endif
                        <a href="{!! $collection_prev['link'] !!}" class="module-single-nav__icon">
                            @include('svgs.left-icon', ['class' => 'module-single-nav__icon-svg'])
                        </a>
                        
                    </div>
                    @endif
                    @if ($collection_next)
                    <div class="col sm-col-2 lg-col-6 module-single-nav__col module-single-nav__col--next scrolled-block__elem">
                        @if ($collection_next['banner_image'])
                        <a href="{!! $collection_next['link'] !!}" class="module-single-nav__image-link" data-hover="true" data-hovertext="View">
                            <figure class="lazy-container module-single-nav__image">
                                <img data-src="{!! $collection_next['banner_image']['sizes']['medium'] !!}" width="{!! $collection_next['banner_image']['sizes']['medium-width'] !!}" height="{!! $collection_next['banner_image']['sizes']['medium-height'] !!}" alt="{!! $collection_next['title'] !!}" class="lazyimage lazyload" />
                            </figure>
                            <h2 class="module-single-nav__title type-h6 type-upper">{!! $collection_next['title'] !!}</h2>
                        </a>
                        @endif
                        <a href="{!! $collection_next['link'] !!}" class="module-single-nav__icon">
                            @include('svgs.right-icon', ['class' => 'module-single-nav__icon-svg'])
                        </a>
                    </div>
                    @endif
                </div>
            </div>
        </section>
        @endif
	@endwhile
	
@endsection
