@extends('layouts.app')

@section('content')
<section class="module module-404 module-padded-top--double module-padded-btm--double">
	<div class="row module-pb-first">
		<div class="col sm-col-4 lg-col-12">
			<h1>404</h1>
			<h3>Sorry, nothing here.</h3>
		</div>
	</div>
</section>
@endsection
