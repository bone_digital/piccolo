<svg xmlns="http://www.w3.org/2000/svg" width="23" height="13" viewBox="0 0 23 13" aria-label="Filter masonary icon"{{ isset($class) ? " class={$class}" : "" }}>
  <g transform="translate(13)">
    <rect width="10" height="5" stroke="none"/>
    <rect x="0.5" y="0.5" width="9" height="4" fill="none"/>
  </g>
  <g>
    <rect width="10" height="5" stroke="none"/>
    <rect x="0.5" y="0.5" width="9" height="4" fill="none"/>
  </g>
  <g transform="translate(13 8)">
    <rect width="10" height="5" stroke="none"/>
    <rect x="0.5" y="0.5" width="9" height="4" fill="none"/>
  </g>
  <g transform="translate(0 8)">
    <rect width="10" height="5" stroke="none"/>
    <rect x="0.5" y="0.5" width="9" height="4" fill="none"/>
  </g>
</svg>

