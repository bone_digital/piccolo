@extends('layouts.app')

@section('content')

	<section class="module module-news-listing module-padded-top--double module-padded-btm--double">
		<div class="row module-pb-first scrolled-block">
			<div class="col sm-col-4 lg-col-12 scrolled-block__elem">
				<h1 class="type-h2">News</h1>
			</div>
		</div>

		<div class="row module-news-listing__filter module-padded-top--single">
			<div class="col sm-col-4 lg-col-12 scrolled-block">
				<div class="module-news-listing__refine scrolled-block__elem">
					<button type="button" class="module-news-listing__refine-toggle{{ $all_categories['current_category'] ? ' module-news-listing__refine-toggle--active' : '' }} lg-hide">@include('svgs.close-icon', ['class' => 'module-news-listing__refine-icon'])Refine</button>
				</div>
				<div class="module-news-listing__cats-wrap{{ $all_categories['current_category'] ? ' module-news-listing__cats-wrap--active' : '' }}">
					<ul class="module-news-listing__cats type-h5 scrolled-block__elem">
						@foreach ($all_categories['categories'] as $cat)
						<li class="module-news-listing__cats-item">
							<a href="{!! $cat->url !!}" class="module-news-listing__cats-link{{ $all_categories['current_category'] == $cat->term_id ? ' module-news-listing__cats-link--active' : '' }}">{!! $cat->name !!}</a>
						</li>
						@endforeach
						<li class="module-news-listing__cats-item">
							<a href="{!! $news_url !!}" class="module-news-listing__cats-link{{ !$all_categories['current_category']? ' module-news-listing__cats-link--active' : '' }}">All posts</a>
						</li>
					</ul>
				</div>
				<ul class="module-news-listing__display scrolled-block__elem content">
					<li class="module-news-listing__display-item sm-hide">
						<button type="button" class="module-news-listing__display-item-btn module-news-listing__display-item-btn--active" data-type="masonry">@include('svgs.filter-masonry-icon', ['class' => 'module-news-listing__display-item-icon'])</button>
					</li>
					<li class="module-news-listing__display-item sm-hide">
						<button type="button" class="module-news-listing__display-item-btn" data-type="stacked">@include('svgs.filter-stack-icon', ['class' => 'module-news-listing__display-item-icon'])</button>
					</li>
				</ul>
			</div>
		</div>

		@if ($posts['masonry'])
		<div class="module-news-listing__list module-news-listing__list--masonry module-news-listing__list--active" data-type="masonry">
			<div class="row scrolled-block">

				@foreach ($posts['masonry'] as $column)
				<div class="col sm-col-4 lg-col-4 module-news-listing__col scrolled-block__elem">
					@foreach ($column as $post)
					<div class="module-news-listing__item content content--thin-underline">
						<time class="type-h5 module-news-listing__date">{!! $post['date'] !!}</time>
						<a href="{!! $post['link'] !!}" class="module-news-listing__link">
							@if ($post['mobile_featured_image'])
							<picture class="lazy-container module-news-listing__image data-hover="true" data-hovertext="View"">
								<source data-srcset="{!! $post['featured_image']['sizes']['large'] !!}" media="(min-width: 901px)">
								<img data-src="{!! $post['mobile_featured_image']['sizes']['medium'] !!}" width="{!! $post['mobile_featured_image']['sizes']['medium-width'] !!}" height="{!! $post['mobile_featured_image']['sizes']['medium-height'] !!}" alt="{!! $post['mobile_featured_image']['alt'] !!}" class="lazyimage lazyload" />
							</picture>
							@else
							<figure class="lazy-container module-news-listing__image data-hover="true" data-hovertext="View"">
								<img data-src="{!! $post['featured_image']['sizes']['large'] !!}" width="{!! $post['featured_image']['sizes']['large-width'] !!}" height="{!! $post['featured_image']['sizes']['large-height'] !!}" alt="{!! $post['featured_image']['alt'] !!}" class="lazyimage lazyload" />
							</figure>
							@endif
							<h2 class="type-p type-underline type-underline--thin module-news-listing__title">{!! $post['title'] !!}</h2>
							@if ($post['excerpt'])
							<p class="module-news-listing__excerpt">{!! $post['excerpt'] !!}</p>
							@endif
							<div class="type-h5 module-news-listing__rm">Read more</div>
						</a>
					</div>
					@endforeach
				</div>
				@endforeach

			</div>
		</div>
		@endif

		@if ($posts['posts'])
		<div class="module-news-listing__list module-news-listing__list--stacked" data-type="stacked">
			@foreach ($posts['posts'] as $post)
			<article class="row module-news-listing__item scrolled-block">
				<div class="col sm-col-4 lg-col-4 module-news-listing__col module-news-listing__col--1 scrolled-block__elem">
					<time class="type-p module-news-listing__date">{!! $post['date'] !!}</time>
				</div>
				<div class="col sm-col-4 lg-col-8 module-news-listing__col module-news-listing__col--2 scrolled-block__elem">
					<a href="{!! $post['link'] !!}" class="module-news-listing__link" data-hover="true" data-hovertext="View">
						@if ($post['mobile_featured_image'] OR $post['featured_image'])
						<div class="module-news-listing__image-wrap">
							@if ($post['mobile_featured_image'])
							<picture class="lazy-container module-news-listing__image">
								<source data-srcset="{!! $post['featured_image']['sizes']['large'] !!}" media="(min-width: 901px)">
								<img data-src="{!! $post['mobile_featured_image']['sizes']['medium'] !!}" width="{!! $post['mobile_featured_image']['sizes']['medium-width'] !!}" height="{!! $post['mobile_featured_image']['sizes']['medium-height'] !!}" alt="{!! $post['mobile_featured_image']['alt'] !!}" class="lazyimage lazyload" />
							</picture>
							@else
							<figure class="lazy-container module-news-listing__image">
								<img data-src="{!! $post['featured_image']['sizes']['large'] !!}" width="{!! $post['featured_image']['sizes']['large-width'] !!}" height="{!! $post['featured_image']['sizes']['large-height'] !!}" alt="{!! $post['featured_image']['alt'] !!}" class="lazyimage lazyload" />
							</figure>
							@endif
						</div>
						@endif
						
						<div class="content content--thin-underline">
							<h2 class="type-p type-underline type-underline--thin module-news-listing__title">{!! $post['title'] !!}</h2>
							@if ($post['excerpt'])
							<p class="module-news-listing__excerpt">{!! $post['excerpt'] !!}</p>
							@endif
							<div class="type-h5 module-news-listing__rm">Read more</div>
						</div>
					</a>
				</div>
			</article>
			@endforeach
		</div>
		@endif

		@if ($posts['paginate'])
		<div class="module-news-listing__more module-news-listing__more--masonry">
			<div class="row scrolled-block">
				<div class="col sm-col-4 lg-col-8 lg-col-offset-4 content content--thin-underline module-news-listing__more-wrap scrolled-block__elem">
					<a href="{!! $posts['paginate'] !!}" class="module-news-listing__more-link type-p">View More</a>
				</div>
			</div>
		</div>
		@endif
	</section>

@endsection