<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    {!! App::googleTagManagerBody() !!}
    @php do_action('get_header') @endphp
    @include('partials.header')
    
    <main id="body-wrapper" class="body-wrapper transition-fade">
      @yield('content')
      @include('partials.footer')
    </main>

    @php do_action('get_footer') @endphp
    @php wp_footer() @endphp
  </body>
</html>
