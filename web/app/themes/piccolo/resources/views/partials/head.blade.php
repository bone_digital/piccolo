<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="facebook-domain-verification" content="am8xx2813r05lzoqfiafhuelqkwcq5" />
	{!! App::googleTagManagerHead() !!}
	@php wp_head() @endphp
</head>
