
<footer class="footer">
	<div class="footer__wrap">
		@if (@!$hide_footer)
		@if ($footer_sign_up['footer_sign_up_gravity_form'])
		<div class="row footer__row footer__row--top">
			<div class="col lg-col-6 sm-col-4 footer__signup-form">
				{!! $footer_sign_up['footer_sign_up_gravity_form_output'] !!}
			</div>
		</div>
		@endif
		@endif
		<div class="row footer__row footer__row--btm">
			<div class="col lg-col-8 sm-col-4">
				<a href="{!! $site_home_url !!}" class="footer__logo-link">
					@include('svgs.piccolo-logo', ['class' => 'footer__logo-link-svg'])
				</a>
			</div>

			@if (has_nav_menu('secondary_navigation'))
			<nav class="footer__nav col lg-col-1 sm-col-1">				
				{!! wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'footer__nav-listing type-h6 type-upper', 'container' => false]) !!}
			</nav>
			@endif

			@if ($piccolo_info['contact_details'])
			<div class="footer__contact-details col lg-col-2 sm-col-2 content content--thin-underline">
				<p class="type-upper type-h6">{!! $piccolo_info['contact_details'] !!}</p>
			</div>
			@endif

			@if ($piccolo_info['socials'])
			<div class="footer__socials col lg-col-1 sm-col-1 content content--thin-underline">
				<p class="type-upper type-h6">
					@foreach ($piccolo_info['socials'] as $social)
					<a href="{!! $social['url'] !!}" target="_blank">{!! $social['title'] !!}</a>
					@if(!$loop->last)
					<br />
					@endif
					@endforeach
				</p>
			</div>
			@endif

			<div class="footer__bbb col lg-col-1 lg-col-offset-11 sm-col-4 content content--thin-underline type-grey">
				<a href="https://bone.digital/" target="_blank" class="type-upper type-h6">Built by Bone</a>
			</div>
		</div>
	</div>
</footer>
