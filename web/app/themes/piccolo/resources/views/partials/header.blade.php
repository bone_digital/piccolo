<div class="site-loader site-loader--active" aria-hidden="true"></div>

<div class="site-cursor" aria-hidden="true">
	<div class="site-cursor__text type-h5 type-upper"></div>
	<div class="site-cursor__icon">
		@include('svgs.close-icon', ['class' => 'site-cursor__icon-close'])
	</div>
</div>
<div class="header-nav-bg" aria-hidden="true" data-hover="true" data-hovericon="close" data-hovertext="Exit"></div>

<header class="header">
	<div class="row header__row">
		<div class="col header__row-inner lg-col-12 sm-col-4">
			<a href="{!! $site_home_url !!}" class="header__logo-link">
				@include('svgs.piccolo-logo', ['class' => 'header__logo-link-svg'])
			</a>

			<button type="button" aria-label="Menu toggle" class="header__nav-btn"></button>

			@if ($piccolo_info['contact_details'] OR $piccolo_info['socials'] OR has_nav_menu('primary_navigation'))
			<nav class="header__nav">

				@if (has_nav_menu('primary_navigation'))
					{!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'header__nav-listing type-h1', 'container' => false]) !!}
				@endif
				
				<div class="header__nav-btm content content--thin-underline">
					@if ($piccolo_info['socials'])
					<p class="type-upper type-h5 header__nav-btm-socials">
						@foreach ($piccolo_info['socials'] as $social)
						<a href="{!! $social['url'] !!}" target="_blank">{!! $social['title'] !!}</a>
						@if(!$loop->last)
						<br />
						@endif
						@endforeach
					</p>
					@endif
				</div>

			</nav>
			@endif
		</div>
	</div>
</header>
