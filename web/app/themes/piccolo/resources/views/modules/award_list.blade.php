<section class="module module-{!! $name !!} module-padded-top--{{ strtolower($padding_top) }} module-padded-btm--{{ strtolower($padding_bottom) }}">
    <div class="{{ ($loop->first AND !is_singular('post'))  ? 'module-pb-first' : '' }}">
        <div class="row module-award_list__header sm-hide scrolled-block">
            <div class="col lg-col-4 scrolled-block__elem">
                <h3 class="type-h4 type-upper">Project</h3>
            </div>
            <div class="col lg-col-1 scrolled-block__elem">
                <h3 class="type-h4 type-upper">Year</h3>
            </div>
            <div class="col lg-col-3 scrolled-block__elem">
                <h3 class="type-h4 type-upper">Award</h3>
            </div>
            <div class="col lg-col-4 scrolled-block__elem">
                <h3 class="type-h4 type-upper">Awarded by</h3>
            </div>
        </div>

        @foreach ($collections as $collection)
            @if ($collection['has_awards_to_display'])
            <div class="scrolled-block{{ (!$loop->first) ? ' module-padded-top--double' : ' module-padded-top--single' }}">
                <div class="row module-award_list__item">
                    <div class="col sm-col-4 lg-col-4 scrolled-block__elem module-award_list__item-details">
                        <h2 class="type-p">{!! $collection['title'] !!}</h2>
                        <a href="{!! $collection['link'] !!}" class="module-award_list__item-image-link" data-hover="true" data-hovertext="View">
                            @if ($collection['banner_media_type'] == 'Image' AND $collection['banner_mobile_media_type'] == 'Image')
                            <picture class="lazy-container module-award_list__item-image">
                                <source srcset="{!! $collection['banner_image']['sizes']['large'] !!}" media="(min-width: 901px)">
                                <img class="lazyimage lazyload" data-src="{!! $collection['banner_mobile_image']['sizes']['medium'] !!}" width="{!! $collection['banner_mobile_image']['sizes']['medium-width'] !!}" height="{!! $collection['banner_mobile_image']['sizes']['medium-height'] !!}" alt="{!! $collection['banner_mobile_image']['alt'] !!}" />
                            </picture>
                            @else
                            
                                @if ($collection['banner_media_type'] == 'Image')
                                <figure class="lazy-container module-award_list__item-image{{ $collection['banner_mobile_media_type']  != 'None' ? " sm-hide" : "" }}">
                                    <img data-src="{!! $collection['banner_image']['sizes']['large'] !!}" width="{!! $collection['banner_image']['sizes']['large-width'] !!}" height="{!! $collection['banner_image']['sizes']['large-height'] !!}" alt="{!! $collection['banner_image']['alt'] !!}" class="lazyimage lazyload" />
                                </figure>
                                @endif

                                @if ($collection['banner_media_type'] == 'Video')
                                <figure class="lazy-container module-award_list__item-image{{ $collection['banner_mobile_media_type']  != 'None' ? " sm-hide" : "" }}">
                                    <video autoplay muted loop playsinline src="{!! $collection['banner_video']['url'] !!}"></video>
                                </figure>
                                @endif

                                @if ($collection['banner_media_type'] == 'Video URL')
                                <figure class="lazy-container module-award_list__item-image{{ $collection['banner_mobile_media_type']  != 'None' ? " sm-hide" : "" }}">
                                    <video autoplay muted loop playsinline src="{!! $collection['banner_video_url'] !!}"></video>
                                </figure>
                                @endif

                                @if ($collection['banner_mobile_media_type'] == 'Image')
                                <figure class="lazy-container module-award_list__item-image lg-hide">
                                    <img data-src="{!! $collection['banner_mobile_image']['sizes']['medium'] !!}" width="{!! $collection['banner_mobile_image']['sizes']['medium-width'] !!}" height="{!! $collection['banner_mobile_image']['sizes']['medium-height'] !!}" alt="{!! $collection['banner_mobile_image']['alt'] !!}" class="lazyimage lazyload" />
                                </figure>
                                @endif

                                @if ($collection['banner_mobile_media_type'] == 'Video')
                                <figure class="lazy-container module-award_list__item-image lg-hide">
                                    <video autoplay muted loop playsinline src="{!! $collection['banner_mobile_video']['url'] !!}"></video>
                                </figure>
                                @endif

                                @if ($collection['banner_mobile_media_type'] == 'Video URL')
                                <figure class="lazy-container module-award_list__item-image lg-hide">
                                    <video autoplay muted loop playsinline src="{!! $collection['banner_mobile_video_url'] !!}"></video>
                                </figure>
                                @endif

                            @endif
                        </a>
                    </div>

                    <div class="col sm-col-4 lg-col-8 module-award_list__item-awards">
                        @foreach ($collection['awards'] as $year)
                            <div class="col sm-col-4 lg-col-1 module-award_list__item-awards-col module-award_list__item-awards-col--year scrolled-block__elem">
                                <h4 class="type-p">{!! $year['year'] !!}</h4>
                            </div>

                            @foreach ($year['awards'] as $award)
                                <div class="col sm-col-4 lg-col-3{{ (!$loop->first) ? ' lg-col-offset-1' : '' }} module-award_list__item-awards-col module-award_list__item-awards-col--award scrolled-block__elem">
                                    @if ($award['link'])
                                    <a href="{!! $award['link'] !!}" class="module-award_list__item-awards-link">
                                    @endif
                                    <p class="type-p">{!! $award['award'] !!}</p>
                                    @if ($award['link'])
                                    </a>
                                    @endif
                                </div>
                                <div class="col sm-col-4 lg-col-4{{ (!$loop->first) ? '' : '' }} module-award_list__item-awards-col module-award_list__item-awards-col--body scrolled-block__elem">
                                    @if ($award['link'])
                                    <a href="{!! $award['link'] !!}" class="module-award_list__item-awards-link">
                                    @endif
                                        <p class="type-p">{!! $award['awarding_body'] !!}</p>
                                    @if ($award['link'])
                                        <span class="module-award_list__item-awards-link-icon" aria-hidden="true"></span>
                                    </a>
                                    @endif
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
        @endforeach
    </div>
</section>
