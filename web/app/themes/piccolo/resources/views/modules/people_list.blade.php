<section class="module module-{!! $name !!} module-padded-top--{{ strtolower($padding_top) }} module-padded-btm--{{ strtolower($padding_bottom) }}">
    @if ($title)
    <div class="row module-people_list__title{{ ($loop->first AND !is_singular('post'))  ? ' module-pb-first' : '' }} module-padded-btm--single scrolled-block">
        <div class="col sm-col-4 lg-col-12 scrolled-block__elem">
            <h2>{!! $title !!}</h2>
        </div>
    </div>
    @endif
    @if ($people)
    <div class="row module-people_list__listing{{ ($loop->first AND !$title) ? ' module-padded-top--single' : '' }} scrolled-block">
        @foreach ($people as $key => $person)
        <div class="col sm-col-4 lg-col-3{{ $key % 3 === 0 ? " lg-col-offset-3" : "" }} module-people_list__item scrolled-block__elem">
            <button type="button" class="module-people_list__btn" data-person="{!! $key !!}" data-hover="true" data-hovertext="View">
                @if ($person['image'])
                <figure class="lazy-container module-people_list__image" style="padding-bottom: {!! $person['image']['sizes']['medium-height'] / $person['image']['sizes']['medium-width'] * 100 !!}%">
                    <img data-src="{!! $person['image']['sizes']['medium'] !!}" width="{!! $person['image']['sizes']['medium-width'] !!}" height="{!! $person['image']['sizes']['medium-height'] !!}" alt="{!! $person['name'] !!}" class="lazyimage lazyload" />
                </figure>
                @endif
                @if ($person['name_type'] == 'Text')
                <h3 class="type-h5">{!! $person['name'] !!}</h3>
                @elseif ($person['name_type'] == 'Image')
                <h3 class="type-h5">
                    <img data-src="{!! $person['name_image']['sizes']['medium'] !!}" width="{!! $person['name_image']['sizes']['medium-width'] !!}" height="{!! $person['name_image']['sizes']['medium-height'] !!}" class="module-people_list__name-image module-people_list__name-image--{!! $key !!} lazyimage lazyload" />
                </h3>
                <style>
                    .module-people_list__name-image--{!! $key !!}
                    {
                        max-width: {!! $person['name_image_max_width'] !!}px;
                        width: 100%;
                        margin-bottom: 0.125rem;
                    }
                </style>
                @endif
                <h4 class="type-h5">{!! $person['job_title'] !!}</h4>
            </button>
            <div class="module-people_list__overlay" data-person="{!! $key !!}">
                <div class="module-people_list__overlay-bg" aria-hidden="true" data-hover="true" data-hovericon="close" data-hovertext="Exit"></div>
                <div class="row module-people_list__overlay-wrap">
                    <div class="col sm-col-4 lg-col-5 module-people_list__overlay-col">
                        <button type="button" class="module-people_list__overlay-close">@include('svgs.close-icon', ['class' => 'module-people_list__overlay-close-icon'])</button>
                        @if ($person['image'])
                        <figure class="lazy-container module-people_list__overlay-image" style="padding-bottom: {!! $person['image']['sizes']['medium-height'] / $person['image']['sizes']['medium-width'] * 100 !!}%">
                            <img data-src="{!! $person['image']['sizes']['medium'] !!}" width="{!! $person['image']['sizes']['medium-width'] !!}" height="{!! $person['image']['sizes']['medium-height'] !!}" alt="{!! $person['name'] !!}" class="lazyimage lazyload" />
                        </figure>
                        @endif
                    </div>
                    <div class="col sm-col-4 lg-col-5 module-people_list__overlay-col">
                        @if ($person['name_type'] == 'Text')
                        <h3 class="type-h4">{!! $person['name'] !!}</h3>
                        @elseif ($person['name_type'] == 'Image')
                        <h3 class="type-h4">
                            <img data-src="{!! $person['name_image']['sizes']['medium'] !!}" width="{!! $person['name_image']['sizes']['medium-width'] !!}" height="{!! $person['name_image']['sizes']['medium-height'] !!}" class="module-people_list__name-image module-people_list__name-image--overlay-{!! $key !!} lazyimage lazyload" />
                        </h3>
                        <style>
                            .module-people_list__name-image--overlay-{!! $key !!}
                            {
                                max-width: {!! $person['name_image_overlay_max_width'] !!}px;
                                width: 100%;
                                margin-bottom: 0.125rem;
                            }
                        </style>
                        @endif
                        <h4 class="type-h4">{!! $person['job_title'] !!}</h4>
                        <div class="content content--thin-underline module-people_list__overlay-bio">
                            {!! str_replace('<p', '<p class="type-h4"', $person['bio'] ) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @endif
</section>