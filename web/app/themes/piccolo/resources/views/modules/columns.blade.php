<section class="module module-{!! $name !!} module-padded-top--{{ strtolower($padding_top) }} module-padded-btm--{{ strtolower($padding_bottom) }} scrolled-block">
	<div class="row{{ ($loop->first AND !is_singular('post')) ? ' module-pb-first' : '' }}">

		@if ($title_area)
		<div class="col sm-col-4 lg-col-12{{ $columns ? ' module-padded-btm--single' : '' }} content content--thin-underline scrolled-block__elem">
			{!! $title_area !!}
		</div>
		@endif

		@if ($columns)

			@foreach ($columns as $column)
			<div class="col {{ ($column['type'] == 'Text' AND $column['caption']) ? 'sm-col-3' : 'sm-col-4' }} lg-col-{!! $column['width'] !!} lg-col-offset-{{ strtolower($column['offset']) }} module-columns__col{{ ($column['show_hide_text'] AND $column['type'] == 'Text') ? ' module-columns__col--text-show-hide' : ''}}{{ $column['show_left_column_border'] ? ' module-columns__col--left-border' : '' }} scrolled-block__elem">
			
				@if ($column['type'] == 'Text')

					@if ($column['show_hide_text'])
					<button type="button" class="module-columns__text-toggle type-h6 type-upper">@include('svgs.close-icon', ['class' => 'module-columns__text-toggle-icon']){!! $column['show_hide_text'] !!}</button>
					@endif
					<div class="module-columns__text content content--thin-underline">
						{!! $column['text'] !!}
					</div>
					@if ($column['caption'])
					<div class="module-columns__caption module-columns__caption--text">
						<p class="type-h6 type-upper">{!! $column['caption'] !!}</p>
					</div>
					@endif

				@elseif ($column['type'] == 'Media')

					@if ($column['media'])

						@if (count($column['media']) > 1 AND $column['media_display'] == 'Slider')
						<div class="module-columns__caption module-columns__caption--images sm-hide">
							<div class="module-columns__slider-captions owl-carousel">
								@foreach ($column['media'] as $media)
									<div data-caption="{!! $loop->iteration !!}">
										@if ($media['caption'])
											<p class="type-h6 type-upper">{!! $media['caption'] !!}</p>
										@endif
									</div>
								@endforeach
							</div>
						</div>
						@endif
						
						<div class="scrolled-block__elem{{ (count($column['media']) > 1 AND $column['media_display'] == 'Slider') ? ' module-columns__slider slider owl-carousel' : ' module-columns__stacked-media' }}">
							@foreach ($column['media'] as $media)
								<div class="module-columns__slider-item{{ $column['media_display'] == 'Stacked' ? ' module-columns__stacked-media-item' : '' }}">
									@php
										$random_string = RandomString()
									@endphp

									@if ($media['caption'] AND (count($column['media']) == 1))
									<div class="module-columns__caption sm-hide">
										<p class="type-h6 type-upper">{!! $media['caption'] !!}</p>
									</div>
									@endif

									@if ($media['media_type'] == 'Image' AND $media['mobile_media_type'] == 'Image')
									<picture class="module-columns__image lazy-container style-{!! $random_string !!}">
										<source data-srcset="{!! $media['image_url'] !!}" media="(min-width: 901px)">
										<img data-src="{!! $media['mobile_image_url'] !!}" width="{!! $media['mobile_image']['sizes']['medium-width'] !!}" height="{!! $media['mobile_image']['sizes']['medium-height'] !!}" alt="{!! $media['mobile_image']['alt'] !!}" class="lazyimage lazyload" />
									</picture>
									<style>
										.module-columns__image.style-{!! $random_string !!}
										{
											padding-bottom: calc({!! $media['mobile_image_height'] !!} / {!! $media['mobile_image_width'] !!} * 100%);
										}
										@media (min-width: 901px)
										{
											.module-columns__image.style-{!! $random_string !!}
											{
												padding-bottom: calc({!! $media['image_height'] !!} / {!! $media['image_width'] !!} * 100%);
											}
										}
									</style>
									@else
									
										@if ($media['media_type'] == 'Image')
										<figure class="lazy-container module-columns__image{{ $media['mobile_media_type'] != 'None' ? " sm-hide" : "" }}" style="padding-bottom: calc({!! $media['image_height'] !!} / {!! $media['image_width'] !!} * 100%)">
											<img data-src="{!! $media['image_url'] !!}" width="{!! $media['image_width'] !!}" height="{!! $media['image_height'] !!}" alt="{!! $media['image']['alt'] !!}" class="lazyimage lazyload" />
										</figure>
										@endif

										@if ($media['media_type'] == 'Video')
										<video class="module-columns__video{{ $media['mobile_media_type'] != 'None' ? " sm-hide" : "" }}" autoplay muted loop playsinline src="{!! $media['video']['url'] !!}"></video>
										@endif

										@if ($media['mobile_media_type'] == 'Image')
										<figure class="lazy-container module-columns__image lg-hide" style="padding-bottom: calc({!! $media['mobile_image_height'] !!} / {!! $media['mobile_image_width'] !!} * 100%)">
											<img data-src="{!! $media['mobile_image_url'] !!}" width="{!! $media['mobile_image_width'] !!}" height="{!! $media['mobile_image_height'] !!}" alt="{!! $media['mobile_image']['alt'] !!}" class="lazyimage lazyload" />
										</figure>
										@endif

										@if ($media['mobile_media_type'] == 'Video')
										<video class="module-columns__video lg-hide" autoplay muted loop playsinline src="{!! $media['mobile_video']['url'] !!}"></video>
										@endif

									@endif
									
								</div>
							@endforeach
						</div>

						@if (count($column['media']) > 1 AND $column['media_display'] == 'Slider')
						<div class="module-columns__slider-nav scrolled-block__elem">
							<button type="button" class="module-columns__slider-nav-btn module-columns__slider-nav-btn--prev">
								@include('svgs.left-icon', ['class' => 'module-columns__slider-nav-btn-icon'])
							</button>
							<button type="button" class="module-columns__slider-nav-btn module-columns__slider-nav-btn--next">
								@include('svgs.right-icon', ['class' => 'module-columns__slider-nav-btn-icon'])
							</button>
						</div>
						@endif
					@endif

				@endif
				
			</div>
			@endforeach
		@endif
	</div>

</section>
