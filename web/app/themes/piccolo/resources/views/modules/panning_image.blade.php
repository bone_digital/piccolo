<section class="module module-{!! $name !!} module-padded-top--{{ strtolower($padding_top) }} module-padded-btm--{{ strtolower($padding_bottom) }}">
	
    <div class="scrolled-block{{ $mobile_image ? " sm-hide" : "" }}">
        <div class="module-panning_image__scroller">
            <figure class="module-panning_image__image-wrap">
                <img data-src="{!! $image['sizes']['large'] !!}" width="{!! $image['sizes']['large-width'] !!}" height="{!! $image['sizes']['large-height'] !!}" alt="{!! $image['alt'] !!}" class="lazyimage lazyload module-panning_image__image" />
            </figure>
        </div>
    </div>

    @if ($mobile_image)
    <div class="lg-hide scrolled-block">
        <figure class="lazy-container scrolled-block__elem module-panning_image__image lg-hide" style="padding-bottom: calc({!! $mobile_image['sizes']['medium-height'] !!} / {!! $mobile_image['sizes']['medium-width'] !!} * 100%)">
            <img data-src="{!! $mobile_image['sizes']['medium'] !!}" width="{!! $mobile_image['sizes']['medium-width'] !!}" height="{!! $mobile_image['sizes']['medium-height'] !!}" alt="{!! $mobile_image['alt'] !!}" class="lazyimage lazyload" />
        </figure>
    </div>
    @endif

</section>
