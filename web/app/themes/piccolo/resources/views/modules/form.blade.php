<section class="module module-{!! $name !!} module-padded-top--{{ strtolower($padding_top) }} module-padded-btm--{{ strtolower($padding_bottom) }}">
    <div class="row module-{!! $name !!}__row{{ ($loop->first AND !is_singular('post')) ? ' module-pb-first' : '' }} scrolled-block">
        <div class="col sm-col-4 lg-col-12 module-{!! $name !!}__col scrolled-block__elem">
            <div class="scrolled-block__elem">
                {!! $gravity_form_code !!}
            </div>
        </div>
    </div>
</section>