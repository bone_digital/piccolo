<section class="module module-{!! $name !!}">
	@if ($media_type == 'Image' AND $mobile_media_type == 'Image')
	<picture class="module-full_screen_media__media module-full_screen_media__media--image">
		<source srcset="{!! $image['sizes']['large'] !!}" media="(min-width: 901px)">
		<img class="lazyimage lazyload" data-src="{!! $mobile_image['sizes']['medium'] !!}" width="{!! $mobile_image['sizes']['medium-width'] !!}" height="{!! $mobile_image['sizes']['medium-height'] !!}" alt="{!! $mobile_image['alt'] !!}" />
	</picture>
	@else
	
		@if ($media_type == 'Image')
		<img data-src="{!! $image['sizes']['large'] !!}" width="{!! $image['sizes']['large-width'] !!}" height="{!! $image['sizes']['large-height'] !!}" alt="{!! $image['alt'] !!}" class="lazyimage lazyload module-full_screen_media__media module-full_screen_media__media--image{{ $mobile_media_type != 'None' ? " sm-hide" : "" }}" />
		@endif

		@if ($media_type == 'Video')
		<video class="module-full_screen_media__media module-full_screen_media__media--video{{ $mobile_media_type != 'None' ? " sm-hide" : "" }}" autoplay muted loop playsinline src="{!! $video['url'] !!}"></video>
		@endif

		@if ($media_type == 'Video URL')
		<video class="module-full_screen_media__media module-full_screen_media__media--video{{ $mobile_media_type != 'None' ? " sm-hide" : "" }}" autoplay muted loop playsinline src="{!! $video_url !!}"></video>
		@endif

		@if ($mobile_media_type == 'Image')
		<img data-src="{!! $mobile_image['sizes']['medium'] !!}" width="{!! $mobile_image['sizes']['medium-width'] !!}" height="{!! $mobile_image['sizes']['medium-height'] !!}" alt="{!! $mobile_image['alt'] !!}" class="lazyimage lazyload module-full_screen_media__media module-full_screen_media__media--image lg-hide" />
		@endif

		@if ($mobile_media_type == 'Video')
		<video class="module-full_screen_media__media--video lg-hide" autoplay muted loop playsinline src="{!! $mobile_video['url'] !!}"></video>
		@endif

		@if ($mobile_media_type == 'Video URL')
		<video class="module-full_screen_media__media--video lg-hide" autoplay muted loop playsinline src="{!! $mobile_video_url !!}"></video>
		@endif

	@endif
	
	@if ($display_scroll_indicator)
	<div class="row module-full_screen_media__scroll-wrap">
		<div class="col sm-col-4 lg-col-12">
			@include('svgs.right-icon', ['class' => 'module-full_screen_media__scroll-svg'])
		</div>
	</div>
	@endif

	<div class="module-full_screen_media__grad" aria-hidden="true"></div>
</section>
