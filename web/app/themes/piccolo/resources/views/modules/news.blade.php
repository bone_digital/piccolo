<section class="module module-{!! $name !!} module-padded-top--{{ strtolower($padding_top) }} module-padded-btm--{{ strtolower($padding_bottom) }}">
	<div class="row module-news__row sm-hide{{ ($loop->first AND !is_singular('post'))  ? ' module-pb-first' : '' }} scrolled-block">
		@foreach ($news as $news_item)
		<div class="col sm-col-4 lg-col-4 module-news__item scrolled-block__elem">
			
			<span class="type-h6 type-upper module-news__item-cats">
				@foreach ($news_item['categories'] as $cat)
					{!! $cat->name !!}{{ !$loop->last ? "," : "" }}
				@endforeach
			</span>

			<a href="{!! $news_item['link'] !!}" class="module-news__item-link" data-hover="true" data-hovertext="View">
				@if ($news_item['featured_image'])
				<div class="module-news__item-image-wrap">
					@if ($news_item['mobile_featured_image'])
					<picture class="lazy-container module-news__item-image">
						<source data-srcset="{!! $news_item['featured_image']['sizes']['large'] !!}" media="(min-width: 901px)">
						<img data-src="{!! $news_item['mobile_featured_image']['sizes']['medium'] !!}" width="{!! $news_item['mobile_featured_image']['sizes']['medium-width'] !!}" height="{!! $news_item['mobile_featured_image']['sizes']['medium-height'] !!}" alt="{!! $news_item['mobile_featured_image']['alt'] !!}" class="lazyimage lazyload" />
					</picture>
					@else
					<figure class="lazy-container module-news__item-image">
						<img data-src="{!! $news_item['featured_image']['sizes']['large'] !!}" width="{!! $news_item['featured_image']['sizes']['large-width'] !!}" height="{!! $news_item['featured_image']['sizes']['large-height'] !!}" alt="{!! $news_item['featured_image']['alt'] !!}" class="lazyimage lazyload" />
					</figure>
					@endif
				</div>
				@endif

				<h2 class="module-news__item-title type-underline">{!! $news_item['title'] !!}</h2>
				@if ($news_item['excerpt'])
				<p class="module-news__item-excerpt">{!! $news_item['excerpt'] !!}</p>
				@endif
			</a>
		</div>
		@endforeach

		<div class="col sm-col-4 lg-col-12 content scrolled-block__elem">
			<a href="{!! $back_to_news !!}" class="type-h2">View more news</a>
		</div>
	</div>

	<div class="row module-news__row lg-hide{{ ($loop->first) ? ' module-padded-top--single' : '' }} scrolled-block">
		<button type="button" class="module-news__mob-next-btn scrolled-block__elem">@include('svgs.right-icon', ['class' => 'module-slider__next-btn-svg'])</button>
		<div class="col sm-col-4 lg-col-12 owl-carousel slider module-news__slider scrolled-block__elem">
			@foreach ($news as $news_item)
			<div class="module-news__item">
				<a href="{!! $news_item['link'] !!}" class="module-news__item-link">
					<span class="type-h6 type-upper module-news__item-cats">
						@foreach ($news_item['categories'] as $cat)
							{!! $cat->name !!}{{ !$loop->last ? "," : "" }}
						@endforeach
					</span>

					@if ($news_item['featured_image'])
						@if ($news_item['mobile_featured_image'])
						<picture class="lazy-container module-news__item-image">
							<source data-srcset="{!! $news_item['featured_image']['sizes']['large'] !!}" media="(min-width: 901px)">
							<img data-src="{!! $news_item['mobile_featured_image']['sizes']['medium'] !!}" width="{!! $news_item['mobile_featured_image']['sizes']['medium-width'] !!}" height="{!! $news_item['mobile_featured_image']['sizes']['medium-height'] !!}" alt="{!! $news_item['mobile_featured_image']['alt'] !!}" class="lazyimage lazyload" />
						</picture>
						@else
						<figure class="lazy-container module-news__item-image">
							<img data-src="{!! $news_item['featured_image']['sizes']['large'] !!}" width="{!! $news_item['featured_image']['sizes']['large-width'] !!}" height="{!! $news_item['featured_image']['sizes']['large-height'] !!}" alt="{!! $news_item['featured_image']['alt'] !!}" class="lazyimage lazyload" />
						</figure>
						@endif
					@endif

					<h2 class="module-news__item-title">{!! $news_item['title'] !!}</h2>
					@if ($news_item['excerpt'])
					<p class="module-news__item-excerpt">{!! $news_item['excerpt'] !!} Read more here.</p>
					@endif
				</a>
			</div>
			@endforeach
		</div>

		<div class="col sm-col-4 lg-col-12 content scrolled-block__elem">
			<a href="{!! $back_to_news !!}" class="type-p">View more news</a>
		</div>
	</div>
</section>