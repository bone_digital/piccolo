<section class="module module-{!! $name !!} module-padded-top--{{ strtolower($padding_top) }} module-padded-btm--{{ strtolower($padding_bottom) }}">
    <div class="row{{ ($loop->first AND !is_singular('post'))  ? ' module-pb-first' : '' }} scrolled-block">
        @if ($title)
		<div class="col sm-col-4 lg-col-12 module-padded-btm--double scrolled-block__elem">
			<h2>{!! $title !!}</h2>
		</div>
		@endif
    </div>

    @if ($collections)
        @foreach ($collections as $collection)
        <div class="row scrolled-block">
            <div class="col sm-col-4 lg-col-12 module-collections__collection module-padded-btm--double scrolled-block__elem">
                @if ($collection['featured_images'])
                <div class="module-collections__collection-slider{{ count($collection['featured_images']) > 1 ? ' module-collections__collection-slider--slider' : ' module-collections__collection-slider--no-slider' }}">
                    @foreach ($collection['featured_images'] as $featured_image)
                    <div class="module-collections__collection-slider-item">
                        <a href="{!! $collection['link'] !!}" data-hover="true" data-hovertext="View">
                            @if ($featured_image['mobile_image'])
                            <picture class="module-collections__collection-slider-image">
                                <source data-srcset="{!! $featured_image['image']['sizes']['large'] !!}" media="(min-width: 901px)">
                                <img data-src="{!! $featured_image['mobile_image']['sizes']['medium'] !!}" width="{!! $featured_image['mobile_image']['sizes']['medium-width'] !!}" height="{!! $featured_image['mobile_image']['sizes']['medium-height'] !!}" alt="{!! $featured_image['mobile_image']['alt'] !!}" class="lazyimage lazyload" />
                            </picture>
                            @else
                            <figure class="module-collections__collection-slider-image">
                                <img data-src="{!! $featured_image['image']['sizes']['large'] !!}" width="{!! $featured_image['image']['sizes']['large-width'] !!}" height="{!! $featured_image['image']['sizes']['large-height'] !!}" alt="{!! $featured_image['image']['alt'] !!}" class="lazyimage lazyload" />
                            </figure>
                            @endif
                        </a>
                    </div>
                    @endforeach
                </div>
                @endif
                @if (count($collection['featured_images']) > 1)
                <button type="button" class="module-collections__collection-nav-over-btn module-collections__collection-nav-over-btn--prev sm-hide" data-hover="true" data-hovertext="Previous"></button>
                <button type="button" class="module-collections__collection-nav-over-btn module-collections__collection-nav-over-btn--next sm-hide" data-hover="true" data-hovertext="Next"></button>
                <div class="module-collections__collection-nav">
                    <button type="button" class="module-collections__collection-nav-btn module-collections__collection-nav-btn--prev">
                        @include('svgs.left-icon', ['class' => 'module-collections__collection-nav-btn-icon'])
                    </button>
                    <button type="button" class="module-collections__collection-nav-btn module-collections__collection-nav-btn--next">
                        @include('svgs.right-icon', ['class' => 'module-collections__collection-nav-btn-icon'])
                    </button>
                </div>
                @endif
                <div class="module-collections__collection-text">
                    <p class="type-h6 type-upper">{!! $collection['sub_heading'] !!}</p>
                    <h3>{!! $collection['title'] !!}</h3>
                </div>
            </div>
        </div>
        @endforeach
    @endif
</section>
