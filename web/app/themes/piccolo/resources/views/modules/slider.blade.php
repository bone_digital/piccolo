<section class="module module-{!! $name !!} module-padded-top--{{ strtolower($padding_top) }} module-padded-btm--{{ strtolower($padding_bottom) }}">
	
    <div class="sm-hide scrolled-block">
        <div class="row module-slider__row">
            <div class="sticky-scroll-section">
                <div class="module-slider__nav scrolled-block__elem">
                    <button type="button" class="module-slider__next-btn">@include('svgs.right-icon', ['class' => 'module-slider__next-btn-svg'])</button>
                </div>
                <div class="col sm-col-4 lg-col-10 module-slider__slider-wrap module-slider__slider-wrap--desk scrolled-block__elem">
                    <div class="module-slider__slider">
                        @foreach ($slider as $slide)
                        <div class="module-slider__slide-wrap">

                            <div class="module-slider__slide{{ !$slide['caption'] ? ' module-slider__slide--no-caption' : '' }}">

                                @if ($slide['caption'])
                                <div class="module-slider__caption">
                                    <p class="type-h6 type-upper">{!! $slide['caption'] !!}</p>
                                </div>
                                @endif

                                @if ($slide['link'])
                                <a class="module-slider__image-link" href="{!! $slide['link']['url'] !!}"{{ $slide['link']['target'] == '_blank' ? ' target="_blank"' : '' }} data-hover="true" data-hovertext="View">
                                @endif

                                @if ($slide['media_type'] == 'Image' AND $slide['mobile_media_type'] == 'Image')
                                <picture class="module-slider__image lazy-container">
                                    <source data-srcset="{!! $slide['image']['sizes']['large'] !!}" media="(min-width: 901px)">
                                    <img data-src="{!! $slide['mobile_image']['sizes']['medium'] !!}" width="{!! $slide['mobile_image']['sizes']['medium-width'] !!}" height="{!! $slide['mobile_image']['sizes']['medium-height'] !!}" alt="{!! $slide['mobile_image']['alt'] !!}" class="lazyimage lazyload" />
                                </picture>
                                @else
                                
                                    @if ($slide['media_type'] == 'Image')
                                    <figure class="module-slider__image lazy-container">
                                        <img data-src="{!! $slide['image']['sizes']['large'] !!}" width="{!! $slide['image']['sizes']['large-width'] !!}" height="{!! $slide['image']['sizes']['large-height'] !!}" alt="{!! $slide['image']['alt'] !!}" class="lazyimage lazyload{{ $slide['mobile_media_type'] != 'None' ? " sm-hide" : "" }}" />
                                    </figure>
                                    @endif

                                    @if ($slide['media_type'] == 'Video')
                                    <figure class="module-slider__video-wrap lazy-container{{ $slide['mobile_media_type'] != 'None' ? " sm-hide" : "" }}">
                                        <video class="module-slider__video" autoplay muted loop playsinline src="{!! $slide['video']['url'] !!}"></video>
                                    </figure>
                                    @endif

                                    @if ($slide['mobile_media_type'] == 'Image')
                                    <figure class="module-slider__image lg-hide">
                                        <img data-src="{!! $slide['mobile_image']['sizes']['medium'] !!}" width="{!! $slide['mobile_image']['sizes']['medium-width'] !!}" height="{!! $slide['mobile_image']['sizes']['medium-height'] !!}" alt="{!! $slide['mobile_image']['alt'] !!}" class="lazyimage lazyload" />
                                    </figure>
                                    @endif

                                    @if ($slide['mobile_media_type'] == 'Video')
                                    <figure class="module-slider__video-wrap lazy-container lg-hide">
                                        <video class="module-slider__video" autoplay muted loop playsinline src="{!! $slide['mobile_video']['url'] !!}"></video>
                                    </figure>
                                    @endif

                                @endif
                                @if ($slide['link'])
                                </a>
                                @endif

                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @if ($link)
                <div class="module-slider__link-wrap content">
                    <a href="{!! $link['url'] !!}"{{ $link['target'] == '_blank' ? ' target="_blank"' : '' }} class="type-h2">{!! $link['title'] !!}</a>
                </div>
                @endif
            </div>
            
        </div>
    </div>

    <div class="row module-slider__row lg-hide scrolled-block">

        <div class="col sm-col-4 lg-col-10 module-slider__slider-wrap module-slider__slider-wrap--mob">
            <div class="module-slider__nav scrolled-block__elem">
                <button type="button" class="module-slider__next-btn">@include('svgs.right-icon', ['class' => 'module-slider__next-btn-svg'])</button>
            </div>
            @if ($link)
            <div class="module-slider__link-wrap content">
                <a href="{!! $link['url'] !!}"{{ $link['target'] == '_blank' ? ' target="_blank"' : '' }} class="type-h2">{!! $link['title'] !!}</a>
            </div>
            @endif
            <div class="slider owl-carousel module-slider__slider scrolled-block__elem">
                @foreach ($slider as $slide)
                <div class="module-slider__slide-wrap">
                     
                    <div class="module-slider__slide">

                        @if ($slide['caption'])
                        <div class="module-slider__caption">
                            <p class="type-h6 type-upper">{!! $slide['caption'] !!}</p>
                        </div>
                        @endif

                        @if ($slide['link'])
                        <a class="module-slider__image-link" href="{!! $slide['link']['url'] !!}"{{ $slide['link']['target'] == '_blank' ? ' target="_blank"' : '' }} data-hover="true" data-hovertext="View">
                        @endif

                        @if ($slide['media_type'] == 'Image' AND $slide['mobile_media_type'] == 'Image')
                        <picture class="module-slider__image lazy-container">
                            <source data-srcset="{!! $slide['image']['sizes']['large'] !!}" media="(min-width: 901px)">
                            <img data-src="{!! $slide['mobile_image']['sizes']['medium'] !!}" width="{!! $slide['mobile_image']['sizes']['medium-width'] !!}" height="{!! $slide['mobile_image']['sizes']['medium-height'] !!}" alt="{!! $slide['mobile_image']['alt'] !!}" class="lazyimage lazyload" />
                        </picture>
                        @else
                        
                            @if ($slide['media_type'] == 'Image')
                            <figure class="module-slider__image lazy-container">
                                <img data-src="{!! $slide['image']['sizes']['large'] !!}" width="{!! $slide['image']['sizes']['large-width'] !!}" height="{!! $slide['image']['sizes']['large-height'] !!}" alt="{!! $slide['image']['alt'] !!}" class="lazyimage lazyload{{ $slide['mobile_media_type'] != 'None' ? " sm-hide" : "" }}" />
                            </figure>
                            @endif

                            @if ($slide['media_type'] == 'Video')
                            <figure class="module-slider__video-wrap lazy-container">
                                <video class="module-slider__video{{ $slide['mobile_media_type'] != 'None' ? " sm-hide" : "" }}" autoplay muted loop playsinline src="{!! $slide['video']['url'] !!}"></video>
                            </figure>
                            @endif

                            @if ($slide['mobile_media_type'] == 'Image')
                            <figure class="module-slider__image lg-hide">
                                <img data-src="{!! $slide['mobile_image']['sizes']['medium'] !!}" width="{!! $slide['mobile_image']['sizes']['medium-width'] !!}" height="{!! $slide['mobile_image']['sizes']['medium-height'] !!}" alt="{!! $slide['mobile_image']['alt'] !!}" class="lazyimage lazyload" />
                            </figure>
                            @endif

                            @if ($slide['mobile_media_type'] == 'Video')
                            <figure class="module-slider__video-wrap lazy-container">
                                <video class="module-slider__video lg-hide" autoplay muted loop playsinline src="{!! $slide['mobile_video']['url'] !!}"></video>
                            </figure>
                            @endif

                        @endif

                        @if ($slide['link'])
                        </a>
                        @endif

                    </div>
                </div>
                @endforeach
            </div>
        </div>

    </div>
</section>
