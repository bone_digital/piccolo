<section class="module module-{!! $name !!} module-padded-top--{{ strtolower($padding_top) }} module-padded-btm--{{ strtolower($padding_bottom) }}">
	<div class="row scrolled-block">
		
		<div class="col sm-col-4 lg-col-12">
					
            <div class="module-full_width_slider__images scrolled-block__elem{{ (count($slider) > 1) ? ' slider owl-carousel' : '' }}">
                @foreach ($slider as $media)
                    <div>
                        @php
                            $random_string = RandomString()
                        @endphp

                        @if ($media['media_type'] == 'Image' AND $media['mobile_media_type'] == 'Image')
                        <picture class="module-full_width_slider__image lazy-container style-{!! $random_string !!}">
                            <source data-srcset="{!! $media['image']['sizes']['large'] !!}" media="(min-width: 901px)">
                            <img data-src="{!! $media['mobile_image']['sizes']['medium'] !!}" width="{!! $media['mobile_image']['sizes']['medium-width'] !!}" height="{!! $media['mobile_image']['sizes']['medium-height'] !!}" alt="{!! $media['mobile_image']['alt'] !!}" class="lazyimage lazyload" />
                        </picture>
                        <style>
                            .module-columns__image.style-{!! $random_string !!}
                            {
                                padding-bottom: calc({!! $media['mobile_image']['sizes']['medium-height'] !!} / {!! $media['mobile_image']['sizes']['medium-width'] !!} * 100%);
                            }
                            @media (min-width: 901px)
                            {
                                .module-columns__image.style-{!! $random_string !!}
                                {
                                    padding-bottom: calc({!! $media['image']['sizes']['large-height'] !!} / {!! $media['image']['sizes']['large-width'] !!} * 100%);
                                }
                            }
                        </style>
                        @else
                        
                            @if ($media['media_type'] == 'Image')
                            <figure class="lazy-container module-full_width_slider__image{{ $media['mobile_media_type'] != 'None' ? " sm-hide" : "" }}" style="padding-bottom: calc({!! $media['image']['sizes']['large-height'] !!} / {!! $media['image']['sizes']['large-width'] !!} * 100%)">
                                <img data-src="{!! $media['image']['sizes']['large'] !!}" width="{!! $media['image']['sizes']['large-width'] !!}" height="{!! $media['image']['sizes']['large-height'] !!}" alt="{!! $media['image']['alt'] !!}" class="lazyimage lazyload" />
                            </figure>
                            @endif

                            @if ($media['media_type'] == 'Video')
                            <video class="module-full_width_slider__video{{ $media['mobile_media_type'] != 'None' ? " sm-hide" : "" }}" autoplay muted loop playsinline src="{!! $media['video']['url'] !!}"></video>
                            @endif

                            @if ($media['mobile_media_type'] == 'Image')
                            <figure class="lazy-container module-full_width_slider__image lg-hide" style="padding-bottom: calc({!! $media['image']['sizes']['medium-height'] !!} / {!! $media['image']['sizes']['medium-width'] !!} * 100%)">
                                <img data-src="{!! $media['mobile_image']['sizes']['medium'] !!}" width="{!! $media['mobile_image']['sizes']['medium-width'] !!}" height="{!! $media['mobile_image']['sizes']['medium-height'] !!}" alt="{!! $media['mobile_image']['alt'] !!}" class="lazyimage lazyload" />
                            </figure>
                            @endif

                            @if ($media['mobile_media_type'] == 'Video')
                            <video class="module-full_width_slider__video lg-hide" autoplay muted loop playsinline src="{!! $media['mobile_video']['url'] !!}"></video>
                            @endif

                        @endif

                    </div>
                @endforeach
            </div>

            @if (count($slider) > 1)
            <div class="module-full_width_slider__nav scrolled-block__elem">
                <button type="button" class="module-full_width_slider__nav-btn module-full_width_slider__nav-btn--prev">
                    @include('svgs.left-icon', ['class' => 'module-full_width_slider__nav-btn-icon'])
                </button>
                <button type="button" class="module-full_width_slider__nav-btn module-full_width_slider__nav-btn--next">
                    @include('svgs.right-icon', ['class' => 'module-full_width_slider__nav-btn-icon'])
                </button>
            </div>
            @endif
		</div>

	</div>

</section>
