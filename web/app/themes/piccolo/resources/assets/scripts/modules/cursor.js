export function cursor_setup()
{

    const cursor = document.querySelector('.site-cursor')

    let clientX = -100
	let clientY = -100

    document.addEventListener('mousemove', e => {
        clientX = e.clientX - 5
        clientY = e.clientY - 5
    })

    const render = () => {
        cursor.style.transform = `translate(${clientX}px, ${clientY}px)`;
        
        requestAnimationFrame(render);
    };
    requestAnimationFrame(render);

}

export function cursor_links()
{
    let hovering = false
    const cursor = document.querySelector('.site-cursor')
    const cursor_text = document.querySelector('.site-cursor__text')

	const cursor_enter = (e) => {
        hovering = true
        cursor.classList.add('site-cursor--hovered')
        e.target.classList.add('type-underline--reverse')
	}

	const cursor_click = () => {
        if (hovering)
        {
            cursor.classList.remove('site-cursor--hovered')

            setTimeout(function() {
                cursor.classList.add('site-cursor--hovered')
            }, 100 )
        }
        else
        {
            cursor.classList.add('site-cursor--hovered')

            setTimeout(function() {
                cursor.classList.remove('site-cursor--hovered')
            }, 100 )
        }
	}
	
	const cursor_leave = (e) => {
        hovering = false
		cursor.classList.remove('site-cursor--hovered')
        e.target.classList.remove('type-underline--reverse')
	}

    const links = document.querySelectorAll('a, button, input, textarea, select')

    document.body.addEventListener('click', cursor_click)

    links.forEach(link => {
        link.addEventListener('mouseenter', cursor_enter)
        link.addEventListener('click', cursor_click)
        link.addEventListener('mouseleave', cursor_leave)
    })

    const hovers = document.querySelectorAll('[data-hover]')

    const cursor_custom_hover_enter = (hover_icon, hover_text) => {
        hovering = true
        if (hover_text)
        {
            cursor.classList.add('site-cursor--text')
            cursor_text.textContent = hover_text
        }
        if (hover_icon)
        {
            cursor.classList.add('site-cursor--icon')
            cursor.classList.add('site-cursor--icon-' + hover_icon)
        }
	}

    const cursor_custom_hover_leave = () => {
        hovering = false
        cursor.className = 'site-cursor'
	}

    hovers.forEach(hover => {

        const hover_icon = hover.dataset.hovericon
        const hover_text = hover.dataset.hovertext

        hover.addEventListener('mouseenter', function(){
            cursor_custom_hover_enter(hover_icon, hover_text)
        })
        hover.addEventListener('mouseleave', cursor_custom_hover_leave)
    })
}

export function reset_cursor()
{
    const cursor = document.querySelector('.site-cursor')
    cursor.className = 'site-cursor'
}