export function setup_menu_trigger()
{
	document.querySelector('.header__nav-btn').addEventListener('click', function(){
		document.body.classList.toggle('header-overlay-open')
		document.body.classList.toggle('overlay-open')
	})

	document.querySelector('.header-nav-bg').addEventListener('click', function(){
		document.body.classList.remove('header-overlay-open')
		document.body.classList.remove('overlay-open')
	})

	document.querySelectorAll('.header__nav-listing a').forEach(function(header_nav_link){
		header_nav_link.addEventListener('click', function(){
			document.body.classList.remove('header-overlay-open')
			document.body.classList.remove('overlay-open')
			update_menu()
		})

		header_nav_link.addEventListener('mouseover', function(){
			document.querySelector('.header__nav-listing').classList.add('header__nav-listing--hovering')
		})

		header_nav_link.addEventListener('mouseout', function(){
			document.querySelector('.header__nav-listing').classList.remove('header__nav-listing--hovering')
		})
	})
}

/**
 * Updates the menu based on the current url
 */
export function update_menu()
{
	const url = window.location.href;

	const current_page = document.querySelector('.header__nav-listing .current-menu-item')
	if (current_page)
	{
		current_page.classList.remove('current-menu-item')
		current_page.querySelector('a').removeAttribute('aria-current')
	}
	
	const active_item = document.querySelector('.header__nav-listing a[href="' + url + '"]')
	if (active_item)
	{
		active_item.setAttribute('aria-current', 'page')
		active_item.parentNode.classList.add('current-menu-item')
	}
	
}
