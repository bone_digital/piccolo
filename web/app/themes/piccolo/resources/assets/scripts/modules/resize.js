import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'

gsap.registerPlugin(ScrollTrigger)

export function resize()
{
	window.addEventListener('resize', function(){
		ScrollTrigger.refresh()
	});
}
