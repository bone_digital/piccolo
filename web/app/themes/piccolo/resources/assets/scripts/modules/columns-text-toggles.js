import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'

gsap.registerPlugin(ScrollTrigger)

export function columns_text_toggles()
{
	const columns_text_toggles = document.querySelectorAll('.module-columns__text-toggle')

	columns_text_toggles.forEach(function(columns_text_toggle){
		let open = false
		columns_text_toggle.addEventListener('click', function(){
			columns_text_toggle.parentNode.classList.toggle('module-columns__col--text-show-hide-open')
			open = !open
			if (open)
			{
				gsap.to(columns_text_toggle.nextElementSibling, .375, { opacity: 1, height: 'auto' })
			}
			else
			{
				gsap.to(columns_text_toggle.nextElementSibling, .375, { opacity: 0, height: 0 })
			}
			ScrollTrigger.refresh()
		})
	})
}
