/*
global renderRecaptcha
global gform
*/
window.onloadCallback = function () {
	renderRecaptcha();
}

export function rerun_gravity_forms_scripts()
{
	//Only find scripts inside the app-container as they are within Swup
	let selector = $('.body-wrapper').find('.gform_wrapper');

	if(selector.length > 0)
	{
		setTimeout(function () {
			let recaptcha_wrap = $('.ginput_recaptcha');
			let recaptcha_iframe = recaptcha_wrap.find('iframe');
			let og_script = 'https://www.google.com/recaptcha/api.js?hl=en&render=explicit&ver=5.2.2';
			let new_script = 'https://www.google.com/recaptcha/api.js?onload=onloadCallback&hl=en&render=explicit&ver=5.2.2';

			if (recaptcha_iframe.length <= 0 && recaptcha_wrap.length > 0) {
				if ($('script[src="' + og_script + '"]').length <= 0 || $('script[src="' + new_script + '"]').length <= 0) {
					$('body').append('<script type="text/javascript" src="' + new_script + '"></script>');
				}

				$(document).on('gform_post_render', function () {
					renderRecaptcha();
				});

			}

		}, 250);

		selector.each(function(){
			let scripts = $(this).parent().find('script');
			scripts.each(function(){
				let text = $(this).text();
				eval(text);
			});
		});
	}
}

export function waitlist_form()
{

	let gform2 = document.querySelector('#gform_2')

	if (gform2)
	{
		gform.addAction( 'gform_input_change', function( elem, formId, fieldId ) {
			if (formId == 2)
			{
				if (elem.value != '')
				{
					elem.parentNode.parentNode.querySelector('.gfield_label').classList.add('gfield_label--field-active')
				}
				else
				{
					elem.parentNode.parentNode.querySelector('.gfield_label').classList.remove('gfield_label--field-active')
				}
			}
		}, 10, 3 );
	}
}
