import Headroom from 'headroom.js'

export function header_scroll()
{
    const header = document.querySelector('.header')
    let headroom  = new Headroom(header);
    headroom.init();
}
