import { cursor_links } from '../modules/cursor'
import { scrolled_blocks } from '../modules/animations'
import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'

gsap.registerPlugin(ScrollTrigger)

export function refine_toggle()
{
    let news_refine_toggle = document.querySelector('.module-news-listing__refine-toggle')

    if (news_refine_toggle)
    {
        let open = false
        let news_cats = document.querySelector('.module-news-listing__cats-wrap')

        news_refine_toggle.addEventListener('click', function(){
            news_refine_toggle.classList.toggle('module-news-listing__refine-toggle--active')

            if (news_cats)
            {
                open = !open
                if (open)
                {
                    gsap.to(news_cats, .375, { opacity: 1, height: 'auto', ease: 'sine.inout' })
                }
                else
                {
                    gsap.to(news_cats, .375, { opacity: 0, height: 0, ease: 'sine.inout' })
                }
                ScrollTrigger.refresh()
            }
        })

        if (news_refine_toggle.classList.contains('module-news-listing__refine-toggle--active'))
        {
            gsap.set(news_cats, { opacity: 1, height: 'auto' })
            open = true
        }
        
    }
}

export function view_more_news()
{
    const more_button = document.querySelector('.module-news-listing__more-link')

    if (more_button)
    {
        more_button.setAttribute('data-no-swup', true)

        let i = 0
        more_button.addEventListener('click', function(e){
            e.preventDefault()

            i++

            more_button.parentNode.classList.add('module-news-listing__more-wrap--loading')

            let xhr = new XMLHttpRequest();

            xhr.open('GET', more_button.href);

            xhr.send();

            xhr.onload = function() {
                if (xhr.status == 200) {

                    jQuery('html, body').animate({
                        scrollTop: jQuery('.module-news-listing__list--active').offset().top + jQuery('.module-news-listing__list--active').height() - jQuery('.header').outerHeight(),
                    }, 1000);

                    // Stacked
                    let dom = document.createElement('div');
                    dom.innerHTML = xhr.response;

                    let posts_container = document.querySelector('.module-news-listing__list--stacked')

                    let unique_class = 'module-news-listing__list--stacked--' + i

                    let el = document.createElement('div')
                    el.style.display = 'none'
                    el.classList.add('module-news-listing__list')
                    el.classList.add('module-news-listing__list--stacked')
                    el.classList.add(unique_class)
                    el.innerHTML = dom.querySelector('.module-news-listing__list--stacked').innerHTML

                    posts_container.innerHTML = posts_container.innerHTML + el.outerHTML

                    jQuery('.' + unique_class).slideDown()

                    // masonry
                    dom = document.createElement('div');
                    dom.innerHTML = xhr.response;

                    posts_container = document.querySelectorAll('.module-news-listing__list--masonry .module-news-listing__col')
                    
                    unique_class = 'module-news-listing__list--masonry--' + i

                    el = document.createElement('div')
                    el.style.display = 'none'
                    el.classList.add('module-news-listing__list')
                    el.classList.add('module-news-listing__list--masonry')
                    el.classList.add(unique_class)

                    let y = dom.querySelectorAll('.module-news-listing__list--masonry .module-news-listing__col')
                    let u = 0
                    posts_container.forEach(function(column){
                        column.innerHTML = column.innerHTML + y[u].innerHTML
                        u++
                    })

                    jQuery('.' + unique_class).slideDown()

                    if (dom.querySelector('.module-news-listing__more'))
                    {
                        more_button.href = dom.querySelector('.module-news-listing__more-link').href
                    }
                    else
                    {
                        jQuery(more_button).slideUp()
                    }

                    more_button.parentNode.classList.remove('module-news-listing__more-wrap--loading')

                    cursor_links()
                    scrolled_blocks()

                    ScrollTrigger.refresh()
                }
            };

        })

    }

}

export function news_display_toggle()
{

    const news_display_item_btns = document.querySelectorAll('.module-news-listing__display-item-btn')

    if (news_display_item_btns.length)
    {
        const news_listings = document.querySelectorAll('.module-news-listing__list')
        const news_more = document.querySelector('.module-news-listing__more')
        const news_display_ls = localStorage.getItem('news-display')

        gsap.set(news_listings, { opacity: 0, display: 'none' })

        // Set Default based on ls
        if (news_display_ls)
        {
            news_display_item_btns.forEach(function(news_display_item_btn_inner){
                if (news_display_item_btn_inner.dataset.type == news_display_ls)
                {
                    news_display_item_btn_inner.classList.add('module-news-listing__display-item-btn--active')
                }
                else
                {
                    news_display_item_btn_inner.classList.remove('module-news-listing__display-item-btn--active')
                }
            })
    
            news_listings.forEach(function(news_listing){
                if (news_listing.dataset.type == news_display_ls)
                {
                    news_listing.classList.add('module-news-listing__list--active')
                    gsap.set(news_listing, { opacity: 1, display: 'block' })
                    if (news_more)
                    {
                        news_more.classList.add('module-news-listing__more--' + news_listing.dataset.type)
                    }
                    localStorage.setItem('news-display', news_listing.dataset.type);
                }
                else
                {
                    news_listing.classList.remove('module-news-listing__list--active')
                    if (news_more)
                    {
                        news_more.classList.remove('module-news-listing__more--' + news_listing.dataset.type)
                    }
                }
            })
        }
        else
        {
            gsap.set(document.querySelector('.module-news-listing__list--active'), { opacity: 1, display: 'block' })
        }

        news_display_item_btns.forEach(function(news_display_item_btn){
            news_display_item_btn.addEventListener('click', function(){

                news_display_item_btns.forEach(function(news_display_item_btn_inner){
                    if (news_display_item_btn_inner.dataset.type == news_display_item_btn.dataset.type)
                    {
                        news_display_item_btn_inner.classList.add('module-news-listing__display-item-btn--active')
                    }
                    else
                    {
                        news_display_item_btn_inner.classList.remove('module-news-listing__display-item-btn--active')
                    }
                })

                gsap.to(news_more, .375, { opacity: 0, display: 'none', ease: 'sine.inout' })

                news_listings.forEach(function(news_listing){
                    if (news_listing.dataset.type == news_display_item_btn.dataset.type)
                    {
                        news_listing.classList.add('module-news-listing__list--active')
                        gsap.to(news_listing, .375, { delay: .4375, opacity: 1, display: 'block', ease: 'sine.inout' })
                        
                        if (news_more)
                        {
                            news_more.classList.add('module-news-listing__more--' + news_listing.dataset.type)
                            gsap.to(news_more, .375, { delay: .4375, opacity: 1, display: 'block', ease: 'sine.inout' })
                        }
                        localStorage.setItem('news-display', news_listing.dataset.type);
                    }
                    else
                    {
                        news_listing.classList.remove('module-news-listing__list--active')
                        gsap.to(news_listing, .375, { opacity: 0, display: 'none', ease: 'sine.inout' })
                        if (news_more)
                        {
                            news_more.classList.remove('module-news-listing__more--' + news_listing.dataset.type)
                        }
                    }
                })

                ScrollTrigger.refresh()
                
            })
        })
    }
}
