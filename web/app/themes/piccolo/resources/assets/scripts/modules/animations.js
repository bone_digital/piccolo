import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'

gsap.registerPlugin(ScrollTrigger)

export function fsm_modules()
{

	let fsm_modules = gsap.utils.toArray('.module-full_screen_media')

	if (fsm_modules.length)
	{
		fsm_modules.forEach(function(fsm_module){

			let fsm_module_grad = fsm_module.querySelector('.module-full_screen_media__grad')

			gsap.to(fsm_module, {
				opacity: 0,
				scrollTrigger: {
					trigger: fsm_module,
					start: 'top top',
					end: 'bottom 25%',
					scrub: true,
				},
			})

			gsap.to(fsm_module_grad, {
				y: '-4rem',
				scrollTrigger: {
					trigger: fsm_module,
					start: 'top top',
					end: 'bottom 50%',
					scrub: true,
				},
			})

		})

	}

}

export function scrolled_blocks()
{

	setTimeout(function(){
	
		let scrolled_blocks = gsap.utils.toArray('.scrolled-block')

		if (scrolled_blocks.length)
		{

			scrolled_blocks.forEach(function(scrolled_block){

				const scrolled_block_elems = scrolled_block.querySelectorAll('.scrolled-block__elem')

				if (scrolled_block_elems)
				{
					gsap.to(scrolled_block, {
						scrollTrigger: {
							trigger: scrolled_block,
							start: 'top 90%',
							onEnter: () => {
								scrolled_block_elems.forEach(function(scrolled_block_elem, index){
									setTimeout(function(){
										scrolled_block_elem.classList.add('scrolled-block__elem--active')
									}, 100 * index)
								})
							},
						},
					})
				}

			})

		}

	}, 500)

}

export function footer_reveal()
{

	let footer = document.querySelector('.footer')

	if (footer)
	{
		gsap.to(footer, {
			scrollTrigger: {
				toggleActions: 'play pause resume reverse',
				toggleClass: {targets: footer, className: 'footer--active'},
				trigger: footer,
				start: 'top bottom-=200',
			},
		})

	}

}

function sticky_slider_sectionss(sticky_slider_section)
{

	let q = sticky_slider_section.querySelectorAll('.module-slider__slider-wrap--desk .module-slider__slide')

	gsap.to(sticky_slider_section.querySelector('.module-slider__slider-wrap'), {
		xPercent: -100, 
		x: () => document.querySelector('.header__row-inner').offsetWidth,
		ease: 'none',
		scrollTrigger: {
			trigger: sticky_slider_section,
			start: 'top top',
			end: () => innerWidth * q.length,
			scrub: true,
			pin: true,
			invalidateOnRefresh: true,
		},
	})
}

function sticky_panning_image(sticky_slider_section)
{

	let q = sticky_slider_section.querySelector('.module-panning_image__image')

	gsap.to(q.parentNode, {
		x: () => -(q.scrollWidth - document.documentElement.clientWidth) + 'px',
		ease: 'none',
		scrollTrigger: {
			trigger: sticky_slider_section,
			start: 'top top',
			end: () => '+=' + q.offsetWidth,
			scrub: true,
			pin: true,
			invalidateOnRefresh: true,
		},
	})
}

export function pinned_sections()
{
	let sticky_slider_sections = gsap.utils.toArray('.module-panning_image__scroller, .sticky-scroll-section')

	if (sticky_slider_sections.length)
	{

		sticky_slider_sections.forEach(function(sticky_slider_section)
		{
			if (sticky_slider_section.classList.contains('sticky-scroll-section'))
			{
				sticky_slider_sectionss(sticky_slider_section)
			}
			else if (sticky_slider_section.classList.contains('module-panning_image__scroller'))
			{
				sticky_panning_image(sticky_slider_section)
			}
		})

	}
}
