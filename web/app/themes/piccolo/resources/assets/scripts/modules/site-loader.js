import { gsap } from 'gsap'

export function setup_site_loader()
{
	let site_loader = document.querySelector('.site-loader')
	
	let tl = gsap.timeline()

	setTimeout(function(){
		document.documentElement.classList.add('site-loaded');
		
		let header_nav_btn = document.querySelector('.header__nav-btn').getBoundingClientRect()

		tl
		.set(site_loader, { yPercent: 0, xPercent: 0, y: (header_nav_btn.y + (header_nav_btn.height / 2)) - (site_loader.offsetHeight / 2), x: (header_nav_btn.x + (header_nav_btn.width / 2)) - (site_loader.offsetWidth / 2) })
		.to(site_loader, 1.5, { scale: 0, ease: 'expo.inOut' })
	}, 1000);
	
}
