import { cursor_links } from '../modules/cursor';

export function people_listing()
{
	const people_btns = document.querySelectorAll('.module-people_list__btn')

	if (people_btns.length)
	{
		people_btns.forEach(function(people_btn){
			people_btn.addEventListener('click', function(){

				let el = document.createElement('div')
				el.innerHTML = document.querySelector('.module-people_list__overlay[data-person="' + people_btn.dataset.person+ '"]').innerHTML
				el.replaceWith(...el.childNodes)
				document.body.appendChild(el);
				el.classList.add('module-people_list__overlay')
				void el.offsetWidth
				el.classList.add('module-people_list__overlay--open')
				document.body.classList.add('people-overlay-open')
				document.body.classList.add('overlay-open')

				const people_overlay = document.querySelector('.module-people_list__overlay--open')

				cursor_links()

				people_overlay.querySelectorAll('.module-people_list__overlay-close, .module-people_list__overlay-bg').forEach(function(close){
					close.addEventListener('click', function(){
						el.classList.remove('module-people_list__overlay--open')
						document.body.classList.remove('people-overlay-open')
						document.body.classList.remove('overlay-open')
						setTimeout(function(){
							el.parentNode.removeChild(el);
						}, 1000)
					})
				})
				
			})
		})
	}
}
