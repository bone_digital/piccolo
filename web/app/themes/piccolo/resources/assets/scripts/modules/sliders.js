import 'owl.carousel'
import Flickity from 'flickity'
import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'
import { cursor_links } from '../modules/cursor';

gsap.registerPlugin(ScrollTrigger)

export function sliders()
{
	let sliders = $('.slider')

	sliders.each(function()
	{
		let options = {
			items: 1,
			center: true,
			autoHeight: true,
			loop: true,
			smartSpeed: 750,
			dots: false,
			animateIn: 'fadeIn',
			animateOut: 'fadeOut',
			touchDrag: false,
			mouseDrag: false,
			pullDrag: false,
		}

		var owl = $(this)

		if ($(this).hasClass('module-collections__collection-slider--slider'))
		{
			options = {
				autoWidth: true,
				center: false,
				loop: true,
				dots: false,
				//lazyLoad: true,
				smartSpeed: 750,
			}
		}

		owl.owlCarousel(options).trigger('refresh.owl.carousel')

		owl.on('initialized.owl.carousel', function(e) {
			cursor_links()
		})

		// Module slider
		$(this).parents('.module-slider__slider-wrap--mob').find('.module-slider__next-btn').click(function() {
			owl.trigger('next.owl.carousel')
		})

		// News slider
		$(this).parents('.module-news').find('.module-news__mob-next-btn').click(function() {
			owl.trigger('next.owl.carousel')
		})

		// Full width slider
		$(this).parents('.module-full_width_slider').find('.module-full_width_slider__nav-btn--prev').click(function() {
			owl.trigger('prev.owl.carousel')
		})

		$(this).parents('.module-full_width_slider').find('.module-full_width_slider__nav-btn--next').click(function() {
			owl.trigger('next.owl.carousel')
		})

		// Columns slider
		var owl_captions = $(this).parents('.module-columns').find('.module-columns__slider-captions')

		options = {
			items: 1,
			center: false,
			loop: true,
			smartSpeed: 750,
			dots: false,
			animateIn: 'fadeIn',
			animateOut: 'fadeOut',
			mouseDrag: false,
			touchDrag: false,
		}

		owl_captions.owlCarousel(options)

		owl.on('changed.owl.carousel', function(e) {
			var index = e.item.index - 1;
			var count = e.item.count;
			if (index > count) {
				index -= count;
			}
			if (index <= 0) {
				index += count;
			}

			owl_captions.trigger('to.owl.carousel', index - 1)
		})

		$(this).parents('.module-columns').find('.module-columns__slider-nav-btn--prev').click(function() {
			owl.trigger('prev.owl.carousel')
		})

		$(this).parents('.module-columns').find('.module-columns__slider-nav-btn--next').click(function() {
			owl.trigger('next.owl.carousel')
		})

		owl.on('refreshed.owl.carousel', function(e) {
			ScrollTrigger.refresh()
		})

		if ($(this).parents('.module-collections__collection')[0])
		{
			setTimeout(function(){
				owl.trigger('refresh.owl.carousel')
			}, 500)
		}

	})

	// Collections slider
	let collections_modules = document.querySelectorAll('.module-collections__collection')

	if (collections_modules.length)
	{
		collections_modules.forEach((collections_module) => {
			let slider = collections_module.querySelector('.module-collections__collection-slider--slider')
			if (slider)
			{
				let flkty = new Flickity(slider, {
					cellAlign: 'left',
					contain: true,
					prevNextButtons: false,
					pageDots: false,
					wrapAround: true,
				});
	
				collections_module.addEventListener('lazyloaded', function(e){
					flkty.resize()
				})

				let prev_btn = collections_module.querySelector('.module-collections__collection-nav-btn--prev')
                let next_btn = collections_module.querySelector('.module-collections__collection-nav-btn--next')
				let prev_btn_over = collections_module.querySelector('.module-collections__collection-nav-over-btn--prev')
                let next_btn_over = collections_module.querySelector('.module-collections__collection-nav-over-btn--next')
					
				prev_btn.addEventListener('click', function() {
					flkty.previous()
				})
	
				next_btn.addEventListener('click', function() {
					flkty.next()
				})
	
				prev_btn_over.addEventListener('click', function() {
					flkty.previous()
				})
	
				next_btn_over.addEventListener('click', function() {
					flkty.next()
				})
			}
		})
		
	}

}
