import { update_menu } from '../modules/menu';
import { sliders } from '../modules/sliders';
import { cursor_links, reset_cursor } from '../modules/cursor';
import { pinned_sections, fsm_modules, scrolled_blocks, footer_reveal } from '../modules/animations';
import { refine_toggle, view_more_news, news_display_toggle } from '../modules/news-listing';
import { people_listing } from '../modules/people-listing';
import { columns_text_toggles } from '../modules/columns-text-toggles';
import reframe from 'reframe.js';
import { waitlist_form } from '../modules/forms';

import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'

gsap.registerPlugin(ScrollTrigger)

export default {
	init() {
		// JavaScript to be fired on all pages
		update_menu()
		sliders()
		cursor_links()
		refine_toggle()
		view_more_news()
		news_display_toggle()
		people_listing()
		columns_text_toggles()
		reframe('iframe')
	},
	finalize() {
		// JavaScript to be fired on all pages, after page specific JS is fired
		reset_cursor()
		fsm_modules()
		scrolled_blocks()
		pinned_sections()
		footer_reveal()
		waitlist_form()
		//orderTriggers();
		ScrollTrigger.sort();
	},
};

/*function orderTriggers() {
	let triggers = ScrollTrigger.getAll();
	triggers.sort((a, b) => a.start - b.start);
	triggers.forEach(trigger => {
		trigger.kill();
		ScrollTrigger.create(trigger.vars);
	});
	ScrollTrigger.refresh();
}*/
