// import external dependencies
import 'jquery';
import 'lazysizes';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';

// Additional Libraries
import { setup_menu_trigger } from './modules/menu';
import { rerun_gravity_forms_scripts } from './modules/forms';
import { cursor_setup } from './modules/cursor';
import { setup_site_loader } from './modules/site-loader';
import { resize } from './modules/resize';
import { header_scroll } from './modules/header';

// Swup setup
import Swup from 'swup';
import SwupBodyClassPlugin from '@swup/body-class-plugin';
import SwupScrollPlugin from '@swup/scroll-plugin';

const options = {
	linkSelector: `a[href^="${window.location.origin}"]:not([data-no-swup]):not([target="_blank"]), a[href^="/"]:not([data-no-swup]):not([target="_blank"]), a[href^="#"]:not([data-no-swup]):not([target="_blank"])`,
	plugins: [
		new SwupBodyClassPlugin(),
		new SwupScrollPlugin({
			doScrollingRightAway: false,
			animateScroll: false,
		}),
	],
	animateHistoryBrowsing: true,
	containers: [
		'#body-wrapper',
	],
};
const swup = new Swup(options);

/** Populate Router instance with DOM routes */
const routes = new Router({
	// All pages
	common,
});

// Generic events - these are loaded once only and would not re-run on Swup page changes
jQuery(document).ready(() => {
	// Place one off code here - such as click events for the menu
	setup_site_loader()
	setup_menu_trigger()
	cursor_setup()
	header_scroll()
	resize()
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());

// Load Events when using Swup
swup.on('contentReplaced', () => {
	rerun_gravity_forms_scripts();
	routes.loadEvents()
});
