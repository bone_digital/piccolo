<?php
namespace App\Piccolo;
use App;

class Collections
{

	public function __construct()
	{
		add_action( 'init', ['App\Piccolo\Collections', 'RegisterPostType'] , 0);
	}

	public static function RegisterPostType()
	{
		$rewrite = array(
			'slug'                  => 'collection',
			'with_front'            => false,
			'pages'                 => true,
			'feeds'                 => true,
		);
		$labels = array(
			'name'                  => _x( 'Collection', 'Post Type General Name', 'piccolo' ),
			'singular_name'         => _x( 'Collection', 'Post Type Singular Name', 'piccolo' ),
		);
		$args = array(
			'label'                 => __( 'Collection', 'piccolo' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'page-attributes' ),
			'hierarchical'          => true,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_icon'				=> 'dashicons-format-gallery',
			'menu_position'         => 5,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => false,
			'rewrite'               => $rewrite,
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
			'map_meta_cap'  => true,
		);
		register_post_type( 'collection', $args );
	}

	public static function GetCollections()
	{
		$collections = [];

		$args = [
			'post_type'	=> 'collection',
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'orderby' => 'menu_order',
			'order'	=> 'asc'
		];

		$query = new \WP_Query($args);

		if($query->have_posts())
		{
			while($query->have_posts())
			{
				$query->the_post();

				$awards = get_field('awards');
				$has_awards_to_display = false;

				if ($awards)
				{
					for ($i = 0; $i < count($awards); $i++)
					{
						$awards[$i]['display_year'] = false;

						foreach ($awards[$i]['awards'] as $award)
						{
							if ($award['display_award_on_collection_page'])
							{
								$has_awards_to_display = true;
								$awards[$i]['display_year'] = true;
								break;
							}
						}
					}
				}

				$collections[] = [
                    'title' => get_the_title(),
                    'link' => get_the_permalink(),
					'sub_heading' => get_field('sub_heading'),
					'banner_media_type' => get_field('banner_media_type'),
					'banner_image' => get_field('banner_image'),
					'banner_video' => get_field('banner_video'),
					'banner_video_url' => get_field('banner_video_url'),
					'banner_mobile_media_type' => get_field('banner_mobile_media_type'),
					'banner_mobile_image' => get_field('banner_mobile_image'),
					'banner_mobile_video' => get_field('banner_mobile_video'),
					'banner_mobile_video_url' => get_field('banner_mobile_video_url'),
					'has_awards_to_display' => $has_awards_to_display,
					'awards' => $awards,
                    'featured_images' => get_field('featured_images'),
                ];
			}
		}

		wp_reset_postdata();

        return $collections;
	}

}
