<?php
namespace App\Piccolo;
use App;

class News
{
	public static function GetPosts()
	{

        $posts = [];
        $paginate = false;

        global $wp_query;

		if(have_posts())
		{
			while(have_posts())
			{
                the_post();
                $posts[] = [
                    'title' => get_the_title(),
                    'link' => get_the_permalink(),
                    'excerpt' => get_field('excerpt'),
                    'categories' => get_the_category(),
                    'date' => get_the_date('d/m/Y'),
                    'featured_image' => get_field('featured_image'),
					'mobile_featured_image' => get_field('mobile_featured_image'),
                ];
            }

            $paginate = next_posts( $wp_query->max_num_pages, false );

        }

        wp_reset_postdata();

        return [
            'posts' => $posts,
            'paginate' => $paginate,
        ];

    }
}
