<?php

namespace App\Controllers;

use App\Bone\PageBuilder;
use Sober\Controller\Controller;

use App\Piccolo\News;

class Index extends Controller
{

	public function Posts()
	{
		$posts = News::GetPosts();

		$columns = [];
		$max_columns = 3;

		for ($i = 0; $i < $max_columns; $i++)
		{
			$columns[$i] = [];
		}
		$current_column = 0;
		foreach ($posts['posts'] as $post)
		{
			$columns[$current_column][] = $post;
			$current_column++;
			if( $current_column >= ($max_columns) )
			{
				$current_column = 0;
			}
		}

		$posts['masonry'] = $columns;

		return $posts;
	}

    public function AllCategories()
	{
		$categories = get_categories();

        foreach ($categories as $cat) :

            if (is_category($cat))
            {
                $cat->active = true;
            }
            else
            {
                $cat->active = false;
            }

            $cat->url = get_category_link($cat);

        endforeach;

		return [
			'categories' => $categories,
			'current_category' => get_query_var('cat')
		];
	}
}
