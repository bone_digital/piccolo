<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
	/**
	 * Returns the image title tag from an ID or image field in ACF
	 *
	 * @param $image
	 * @return mixed|string
	 */
	public static function getImageTitle($image)
	{
		$title = '';
		if( null == $image )
		{
			//Empty
			return '';
		}

		if( is_array($image) )
		{
			if( array_key_exists('title', $image) )
			{
				$title = $image['title'];
			}
		}
		else if( is_integer($image) )
		{
			$title = get_the_title($image);
		}

		return $title;
	}

	/**
	 * Returns the image alt tag from an image ID or image field in ACF
	 *
	 * @param $image
	 * @return mixed|string
	 */
	public static function getImageAlt($image)
	{
		$alt = '';
		if( null == $image )
		{
			//Empty
			return '';
		}

		if( is_array($image) )
		{
			if( array_key_exists('alt', $image) )
			{
				$alt = $image['alt'];
			}
		}
		else if( is_integer($image) )
		{
			$alt = get_post_meta( $image, '_wp_attachment_image_alt', true );
		}

		return $alt;
	}

	/**
	 * Generates an img tag with all attributes required
	 * @param $image
	 * @param string $size
	 * @param string $classes
	 * @return string
	 */
	public static function generateImgTag($image, $size = 'full', $classes = '')
	{
		$img_html = '';
		if( !empty($image) )
		{
			$image = $image['ID'];
			$title = App::getImageTitle($image);
			$alt = App::getImageAlt($image);
			$image_url = App::getImageUrlFromField($image, $size);
			$img_html = '<img class="' . $classes . '" title="'. $title . '" alt="' . $alt . '" src="' . $image_url . '" />';
		}
		return $img_html;
	}

	/**
	 * Returns a url from a field or attachment ID
	 *
	 * @param $image ID or image field
	 * @param string $size size of the image
	 * @return mixed|string|void image url or null if can't be found
	 */
	public static function getImageUrlFromField($image, $size = 'full')
	{
		$image_url = '';
		if( null == $image || empty($image) )
		{
			//Doesn't exist
			return;
		}

		if( is_array($image) && !empty($image) )
		{
			if( 'full' == $size )
			{
				$image_url = $image['url'];
			}
			else
			{
				$image_url = $image['sizes'][$size];
			}
		}
		else if( is_integer($image) )
		{
			$obj = wp_get_attachment_image_src($image, $size);
			if( !is_wp_error($obj) )
			{
				$image_url = $obj[0];
			}
		}

		return $image_url;
	}

	/**
	 * Returns the site name
	 *
	 * @return string|void
	 */
    public function siteName()
    {
        return get_bloginfo('name');
    }

	/**
	 * Returns the title based on teh type of page or archive
	 *
	 * @return string|void
	 */
    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

	/**
	 * Outputs the HTML for GA
	 *
	 * @return string|string[]
	 */
    public static function googleTagManagerHead()
    {
        $tm_code = get_field('tm_code', 'options');
        $html = '';
        if( !empty($tm_code) )
        {
            $html = "<!-- Google Tag Manager -->
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','{{CODE}}');</script>
			<!-- End Google Tag Manager -->";
            $html = str_replace('{{CODE}}', $tm_code, $html);
        }
        return $html;
    }

	public static function googleTagManagerBody()
    {
        $tm_code = get_field('tm_code', 'options');
        $html = '';
        if( !empty($tm_code) )
        {
            $html = "<!-- Google Tag Manager (noscript) -->
			<noscript><iframe src=https://www.googletagmanager.com/ns.html?id={{CODE}}
			height='0' width='0' style='display:none;visibility:hidden'></iframe></noscript>
			<!-- End Google Tag Manager (noscript) -->";
            $html = str_replace('{{CODE}}', $tm_code, $html);
        }
        return $html;
    }

    public function siteHomeUrl()
    {
        return home_url();
    }

	public function newsUrl()
    {
        return get_permalink( get_option( 'page_for_posts' ) );
    }

	public function piccoloInfo()
    {
        $contact_details = get_field('contact_details', 'options');
		$socials = get_field('socials', 'options');

		return [
			'contact_details' => strip_tags($contact_details, '<br><a><i><em>'),
			'socials' => $socials,
		];
    }

	public function footerSignUp()
	{

		$footer_sign_up_gravity_form = get_field('footer_sign_up_gravity_form', 'options');

		$footer_sign_up_gravity_form_output = gravity_form( $footer_sign_up_gravity_form, true, false, false, null, true, 0, false );

		return [
			'footer_sign_up_gravity_form' => $footer_sign_up_gravity_form,
			'footer_sign_up_gravity_form_output' => $footer_sign_up_gravity_form_output,
		];

	}
}
