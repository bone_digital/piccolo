<?php

namespace App\Controllers;

use App\Bone\PageBuilder;
use Sober\Controller\Controller;

class Single extends Controller
{
	public function Layouts()
	{
		//Using Sections
		//$page_builder = new PageBuilder(get_field('sections'), true );

		//Using Modules
		$page_builder = new PageBuilder(get_field('modules'), false );

		return $page_builder->layouts();
	}

	public function Post()
	{
		$post = [
			'title' => get_the_title(),
			'categories' => get_the_category(),
		];

		return $post;
    }

	public function BackToNews() {
		$back_to_news = get_the_permalink(get_option('page_for_posts'));

		return $back_to_news;
	}
}
