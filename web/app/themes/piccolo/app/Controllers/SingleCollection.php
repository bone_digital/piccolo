<?php

namespace App\Controllers;

use App\Bone\PageBuilder;
use Sober\Controller\Controller;

use App\Piccolo\Collections;

class SingleCollection extends Controller
{
	public function Layouts()
	{
		//Using Sections
		//$page_builder = new PageBuilder(get_field('sections'), true );

		//Using Modules
		$page_builder = new PageBuilder(get_field('modules'), false );

		return $page_builder->layouts();
	}

	public function Collection()
	{
        $awards = get_field('awards');
        $has_awards_to_display = false;

        if ($awards)
        {
            for ($i = 0; $i < count($awards); $i++)
            {
                $awards[$i]['display_year'] = false;

                foreach ($awards[$i]['awards'] as $award)
                {
                    if ($award['display_award_on_collection_page'])
                    {
                        $has_awards_to_display = true;
                        $awards[$i]['display_year'] = true;
                        break;
                    }
                }
            }
        }
        
		$post = [
			'title' => get_the_title(),
            'sub_heading' => get_field('sub_heading'),
            'description' => get_field('description'),
            'awards_heading' => get_field('awards_heading'),
            'has_awards_to_display' => $has_awards_to_display,
            'awards' => $awards,
			'banner_media_type' => get_field('banner_media_type'),
            'banner_image' => get_field('banner_image'),
            'banner_video' => get_field('banner_video'),
            'banner_video_url' => get_field('banner_video_url'),
            'banner_mobile_media_type' => get_field('banner_mobile_media_type'),
            'banner_mobile_image' => get_field('banner_mobile_image'),
            'banner_mobile_video' => get_field('banner_mobile_video'),
            'banner_mobile_video_url' => get_field('banner_mobile_video_url'),
		];

		return $post;
    }

    public function CollectionPrev()
    {
        if (get_adjacent_post(false, '', false))
        { 
            $next_post = get_next_post();

            $next_post = [
                'link' => get_the_permalink($next_post->ID),
                'title' => get_the_title($next_post->ID),
                'banner_image' => get_field('banner_image', $next_post->ID),
            ];
        }
        else
        { 
            $first = new \WP_Query(array(
                'post_type' => array('collection'),
                'order' => 'DESC',
                'orderby' => 'menu_order',
                'post__not_in' => array (get_the_ID()),
                'posts_per_page' => 1
            ));
            $first->the_post();
            $next_post = [
                'link' => get_the_permalink(),
                'title' => get_the_title(),
                'banner_image' => get_field('banner_image'),
            ];
            wp_reset_query();
        }

        return $next_post;
    }

    public function CollectionNext()
    {
        
        if (get_adjacent_post(false, '', true))
        { 
            $prev_post = get_previous_post();

            $prev_post = [
                'link' => get_the_permalink($prev_post->ID),
                'title' => get_the_title($prev_post->ID),
                'banner_image' => get_field('banner_image', $prev_post->ID),
            ];
        }
        else
        {
            $last = new \WP_Query(array(
                'post_type' => array('collection'),
                'order' => 'ASC',
                'orderby' => 'menu_order',
                'post__not_in' => array (get_the_ID()),
                'posts_per_page' => 1
            ));
            $last->the_post();
            $prev_post = [
                'link' => get_the_permalink(),
                'title' => get_the_title(),
                'banner_image' => get_field('banner_image'),
            ];
            wp_reset_query();
        }
        
        return $prev_post;
    }
}
