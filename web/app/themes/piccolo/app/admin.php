<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Whitelabel Infinite WP client
 */
add_action('init', function() {
	if( !is_admin() || !isset($_GET['bone_setup_iwp']) )
	{
		return;
	}

	//Update the client
	$iwp_options = [
		'doChangesCPB' => true,
		'name'    => 'Bone Monitoring',
		'title'    => 'Bone Monitoring',
		'desc'    => 'Adds the ability to monitor the site for Bone clients on maintenance plans',
		'author_url'    => 'https://bone.digital/',
		'author'    => 'Bone Digital',
		'hide'    => false,
	];

	update_option('iwp_client_brand', $iwp_options);
});

/**
 * Removes the dashboard widgets form the admin area
 */
add_action('wp_dashboard_setup', function() {
	global $wp_meta_boxes;
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_site_health']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});

/**
 * ACF Options Page
 */
add_action('init', function () {
    if( function_exists('acf_add_options_page') )
    {
        acf_add_options_page();
    }
});
