<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

use App\Bone\CookieNotice;
use App\Bone\Admin\Maintenance;
use App\Bone\Security;
use App\Bone\Theme;
use App\Bone\Emails;
use App\Bone\Admin\ACF;

use App\Piccolo\News;
use App\Piccolo\Collections;

new ACF();
new Theme();
new Security();
new Emails();
new Maintenance();
new Ajax();
new CookieNotice();
new News();
new Collections();

/**
 * Setup Image Sizes
 *
 * add_image_size($name, $width, $height, $crop)
 */
//add_image_size('image_size_name', 300, 300, false);
//add_image_size('card', 1200, 1200, false);

/**
 * Add font preloads, favicons and extra admin page styling
 */
add_action('wp_head', function(){
?>
    <link rel='icon' href="<?=asset_path('images/icons/favicon.svg')?>" />
    <link rel="shortcut icon" href="<?=asset_path('images/icons/favicon.png')?>" type="image/png">
    <link rel="apple-touch-icon" href="<?=asset_path('images/icons/apple-touch-icon.png')?>" type="image/png">

    <link rel="preload" href="<?=asset_path('fonts/EuclidCircularB-Regular-WebS.woff2')?>" as="font" crossorigin>
    <link rel="preload" href="<?=asset_path('fonts/EuclidCircularB-RegularItalic-WebS.woff2')?>" as="font" crossorigin>
    <link rel="preload" href="<?=asset_path('fonts/EuclidCircularB-Light-WebS.woff2')?>" as="font" crossorigin>
    <link rel="preload" href="<?=asset_path('fonts/EuclidCircularB-LightItalic-WebS.woff2')?>" as="font" crossorigin>
<?php
}, 1);

add_filter('show_admin_bar', '__return_false');

/**
 * Disable Gutenberg
 */
add_filter('use_block_editor_for_post', '__return_false');

/**
 * Adds a funciton missing for Captcha with Swup
 */
add_action('wp_head', function() {
?>
	<script>
		function onloadCallback()
		{
			renderRecaptcha();
		}
	</script>
<?php
});

/**
 * Remove Default Image Sizes
 */
function remove_default_image_sizes( $sizes )
{
	// Keep the thumbnail size for the media library
	// unset( $sizes['thumbnail']);
	//unset( $sizes['medium']);
	unset( $sizes['medium_large']);
	//unset( $sizes['large']);

	return $sizes;
}
//add_filter('intermediate_image_sizes_advanced', 'App\remove_default_image_sizes');

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    $version = null;
    $environment = env('WP_ENV');

    //Use the date/time as the version number on staging and development environments to avoid caching
    if('production' != $environment)
    {
        $version = date('YmdHis');
    }

    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, $version);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], $version, true);
	wp_localize_script('sage/main.js', 'Ajax', [
		'url'	=> admin_url('admin-ajax.php')
	]);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage'),
        'secondary_navigation' => __('Secondary Navigation', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer', 'sage'),
        'id'            => 'sidebar-footer'
    ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Provide the editor with access to manage the menus
 */
add_action( 'after_switch_theme', function() {
	$role = get_role('editor');
	$role->add_cap( 'gform_full_access' );
	$role->add_cap( 'edit_theme_options' );
	$role->add_cap( 'manage_options' );
});

/**
 * Setup ACF Pro Key
 */
add_action( 'after_switch_theme', function() {
	if ( ! get_option( 'acf_pro_license' ) && defined( 'ACF_PRO_KEY' ) )
	{
		$save = array(
			'key'	=> ACF_PRO_KEY,
			'url'	=> home_url()
		);

		$save = maybe_serialize($save);
		$save = base64_encode($save);

		update_option( 'acf_pro_license', $save );
	}
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

/**
 * Disables the password changed email to the administrator
 */
if( function_exists( 'get_field' ) )
{
	if( get_field('disable_send_password_change_notifications', 'options') )
	{
		if( !function_exists( 'wp_password_change_notification' ) )
		{
			function wp_password_change_notification($user)
			{
				return;
			}
		}
	}
}

// Change Posts to News in admin
add_action( 'init', 'App\cp_change_post_object' );
function cp_change_post_object() {
    $get_post_type = get_post_type_object('post');
    $labels = $get_post_type->labels;
    $labels->name = 'News';
    $labels->singular_name = 'News';
    $labels->add_new = 'Add News';
    $labels->add_new_item = 'Add News';
    $labels->edit_item = 'Edit News';
    $labels->new_item = 'News';
    $labels->view_item = 'View News';
    $labels->search_items = 'Search News';
    $labels->not_found = 'No News found';
    $labels->not_found_in_trash = 'No News found in Trash';
    $labels->all_items = 'All News';
    $labels->menu_name = 'News';
    $labels->name_admin_bar = 'News';
}

// Hide tags page in admin
add_action('admin_menu', 'App\remove_post_menus');
function remove_post_menus() {
    remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');
}

// Hide tags metabox
add_action('admin_menu','App\remove_post_metaboxes');
function remove_post_metaboxes() {
    remove_meta_box( 'tagsdiv-post_tag', 'post',' normal' );
}

// Remove comments admin bar tab
add_action( 'wp_before_admin_bar_render', 'App\cleanup_wp_admin_bar' );
function cleanup_wp_admin_bar() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}

function my_manage_columns( $columns ) {
    unset($columns['tags']);
    return $columns;
}

function my_column_init() {
    add_filter( 'manage_posts_columns' , 'App\my_manage_columns' );
}

add_action( 'admin_init' , 'App\my_column_init' );
