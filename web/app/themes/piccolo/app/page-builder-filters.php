<?php

namespace App;

use App\Piccolo\Collections;

function news_module_filter($module)
{

	$back_to_news = get_the_permalink(get_option('page_for_posts'));

	$news = [];

	foreach ($module['news'] as $news_item)
	{
		$news[] = [
			'link' => get_the_permalink($news_item),
			'title' => get_the_title($news_item),
			'excerpt' => get_field('excerpt', $news_item),
			'categories' => get_the_category($news_item),
			'featured_image' => get_field('featured_image', $news_item),
			'mobile_featured_image' => get_field('mobile_featured_image', $news_item),
		];
	}

	$module['back_to_news'] = $back_to_news;
	$module['news'] = $news;

	return $module;
}
add_filter('page_builder_module_news', 'App\news_module_filter', 10, 1);

function form_module_filter($module)
{

	$gravity_form_id = $module['gravity_form_id'];
	$gravity_form_code = gravity_form( $gravity_form_id, true, false, false, null, true, 0, false );

	$module['gravity_form_code'] = $gravity_form_code;

	return $module;
}
add_filter('page_builder_module_form', 'App\form_module_filter', 10, 1);

function collections_module_filter($module)
{

	if ($module['collections'])
	{
		$collections = [];

		foreach ($module['collections'] as $collection)
		{
			$awards = get_field('awards', $collection);
			$has_awards_to_display = false;

			if ($awards)
			{
				for ($i = 0; $i < count($awards); $i++)
				{
					$awards[$i]['display_year'] = false;

					foreach ($awards[$i]['awards'] as $award)
					{
						if ($award['display_award_on_collection_page'])
						{
							$has_awards_to_display = true;
							$awards[$i]['display_year'] = true;
							break;
						}
					}
				}
			}

			$collections[] = [
				'title' => get_the_title($collection),
				'link' => get_the_permalink($collection),
				'sub_heading' => get_field('sub_heading', $collection),
				'banner_image' => get_field('banner_image', $collection),
				'banner_mobile_image' => get_field('banner_mobile_image', $collection),
				'has_awards_to_display' => $has_awards_to_display,
				'awards' => $awards,
				'featured_images' => get_field('featured_images', $collection),
			];
		}

		$module['collections'] = $collections;
	}

	return $module;
}
add_filter('page_builder_module_collections', 'App\collections_module_filter', 10, 1);

function award_list_module_filter($module)
{

	$collections = Collections::GetCollections();
	$module['collections'] = $collections;

	return $module;
}
add_filter('page_builder_module_award_list', 'App\award_list_module_filter', 10, 1);

function columns_module_filter($module)
{

	$i = 0;
	foreach ($module['columns'] as $column)
	{
		if ($column['media'])
		{
			$q = 0;
			foreach ($column['media'] as $media)
			{

				if ($media['media_type'] == 'Image' && $media['image'])
				{
					if ($media['image']['mime_type'] == 'image/gif')
					{
						$module['columns'][$i]['media'][$q]['image_url'] = $media['image']['url'];
						$module['columns'][$i]['media'][$q]['image_width'] = $media['image']['width'];
						$module['columns'][$i]['media'][$q]['image_height'] = $media['image']['height'];
					}
					else
					{
						$module['columns'][$i]['media'][$q]['image_url'] = $media['image']['sizes']['large'];
						$module['columns'][$i]['media'][$q]['image_width'] = $media['image']['sizes']['large-width'];
						$module['columns'][$i]['media'][$q]['image_height'] = $media['image']['sizes']['large-height'];
					}
				}


				if ($media['mobile_media_type'] == 'Image')
				{
					if ($media['mobile_image']['mime_type'] == 'image/gif')
					{
						$module['columns'][$i]['media'][$q]['mobile_image_url'] = $media['mobile_image']['url'];
						$module['columns'][$i]['media'][$q]['mobile_image_width'] = $media['mobile_image']['width'];
						$module['columns'][$i]['media'][$q]['mobile_image_height'] = $media['mobile_image']['height'];
					}
					else
					{
						$module['columns'][$i]['media'][$q]['mobile_image_url'] = $media['mobile_image']['sizes']['medium'];
						$module['columns'][$i]['media'][$q]['mobile_image_width'] = $media['mobile_image']['sizes']['medium-width'];
						$module['columns'][$i]['media'][$q]['mobile_image_height'] = $media['mobile_image']['sizes']['medium-height'];
					}
				}

				$q++;
			}
		}

		$i++;
	}

	return $module;
}
add_filter('page_builder_module_columns', 'App\columns_module_filter', 10, 1);
