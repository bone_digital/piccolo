<?php

namespace App\Bone\Admin;

class ACF
{
    public function __construct()
    {
        add_filter( 'acf/settings/save_json', array( $this, 'get_local_json_path' ) );
        add_filter( 'acf/settings/load_json', array( $this, 'add_local_json_path' ) );
        if ( 'development' !== env('WP_ENV')  )
        {
            // Only allow fields to be edited on development
            add_filter( 'acf/settings/show_admin', '__return_false' );
        }
    }

    /**
     * Returns the directory 'app/data'
     *
     * @return string
     */
    public function get_data_dir()
    {
        $url = WP_CONTENT_DIR . '/data/';
        return $url;
    }

    /**
     * Define where the local JSON is saved
     *
     * @return string
     */
    public function get_local_json_path()
    {
        return $this->get_data_dir() . 'acf-json';
    }

    /**
     * Add our path for the local JSON
     *
     * @param array $paths
     *
     * @return array
     */
    public function add_local_json_path( $paths )
    {
        $paths[] = $this->get_data_dir() . 'acf-json';
        return $paths;
    }
}
